import React from "react";


class GoogleRedirect extends React.Component {


    getCookies() {
        //STARTER && BREAKER : emergency break to the for 
        let starter = 0
        let breaker = 10;
        let rawCookies = document.cookie + ";";


        for (let i = 0; i = rawCookies.length; i) {
            const indexPropertyName = rawCookies.indexOf('=');
            const indexPropertyValueFinal = rawCookies.indexOf(';');
            const propertyName = rawCookies.substring(0, indexPropertyName);
            const propertyValue = rawCookies.substring(indexPropertyName + 1, indexPropertyValueFinal);
            window.sessionStorage.setItem(propertyName, decodeURIComponent(propertyValue))
            const cleaner = rawCookies.substring(0, indexPropertyValueFinal + 1);
            rawCookies = rawCookies.replace(cleaner, "");
            rawCookies = rawCookies.replace(" ", "");
            starter += 1
            if (starter === breaker) {
                console.log(starter);
                return false
            }
        }
        this.moveTo()
    }

    moveTo = () => {
        window.location.href = '/home'
    }

    render() {
        return <div>
            REDIRECCIONANDO...
            {this.getCookies()}
        </div>
    }
}



export default GoogleRedirect;

