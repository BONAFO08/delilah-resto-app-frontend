import React from "react";
import urlImgs from "./login.background.imgs.json"

import './login.index.css'
class Login extends React.Component {

    state = {
        name: '',
        password: '',

    }

    login = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let requestOptions = {
            method: 'POST',
            body: JSON.stringify(dataInput),
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/logIn`, requestOptions)
            .then(response => resp = response.json())
            .catch(error => console.log('error', error));
        return resp;
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        const validator =
            this.state.name.trim().length !== 0 &&
            this.state.password.trim().length !== 0;
        if (validator === true) {
            const response = this.login(this.state)
                .then(response => {
                    if (response.status === 200) {
                        window.sessionStorage.setItem('token', response.token);
                        window.sessionStorage.setItem('sessionID', response.sessionID);
                        window.sessionStorage.setItem('userImg', response.userImg);
                        window.location.href = '/home';

                    } else {
                        document.getElementById('response-login-h3').style.display = 'inline';
                        document.getElementById('response-login-h3').style.color = 'red';
                    }
                })
                .catch(error => {
                    document.getElementById('response-login-h3').style.display = 'inline';
                    document.getElementById('response-login-h3').style.color = 'red';
                });
        } else {
            document.getElementById('response-login-h3').style.display = 'inline';
            document.getElementById('response-login-h3').style.color = 'red';
        }

    }


    changin = (e) => {
        //PARA CAMBIAR SI NO TIENES PERMITIDO O NO TENES TOKEN
        e.target.setAttribute('href', '/')
    }
    render() {
        return <div className="background-login"
            style={{ backgroundImage: `url("${urlImgs.imgs[2]}")` }}>
            <div className="card-login">
                <form onSubmit={this.onSubmit} method="POST">
                    <h3 id="title-login" className="h3-login">DELILAH RESTO</h3>
                    <br />

                    <h3 className="h3-login">
                        NOMBRE DE USUARIO O CORREO ELECTRONICO</h3>
                    <br />
                    <input required id="username-login-input" className="input-login" type='text' name="name" value={this.state.name} onChange={this.onChange}
                    />

                    <h3 className="h3-login">CONTRASEÑA</h3>
                    <br />
                    <input required id="username-login-input" className="input-login" type='password' name="password" value={this.state.password} onChange={this.onChange} />

                    <br />
                    <h3 id="response-login-h3" className="h3-login"
                    >USUARIO O CONTRASEÑA
                        <br />
                        INCORRECTA</h3>

                    <a className="button-login" href="/" style={{ margin: "16vh 0vh 0vh 10vh" }}
                    >VOLVER</a>

                    <input className="button-login" id="btn-send-login" type='submit' value='ENVIAR'
                    />

                    <a className="button-login" href="/signup" style={{ margin: "29vh 0vh 0vh 14vh" }}
                    >NO TENGO CUENTA</a>

                </form>
            </div>
        </div>
    }
}



export default Login;


