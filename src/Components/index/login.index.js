import React from "react";
import urlImgs from "./login.background.imgs.json"


import './login.index.css'
class LoginIndex extends React.Component {

    selectImgBackground = () => {
        return Math.floor(Math.random() * urlImgs.imgs.length);
    }

    render() {
        return <div className="background-login"
            style={{ backgroundImage: `url("${urlImgs.imgs[this.selectImgBackground()]}")` }}>
            <div className="card-login">

                <h3 id="title-login" className="h3-login">DELILAH RESTO</h3>
                <br />

                <a className="button-login" href="/signup" style={{ margin: "5vh 0vh 0vh 16vh" }}>
                    CREAR CUENTA</a>
                <br />

                <a className="button-login" href="/login" style={{ margin: "23vh 0vh 0vh 16vh" }}>
                    INICIAR SESIÓN</a>
                <br />

                <a className="thirdLogin-login " id="google"
                    href="http://localhost:3000/auth/google/callback"
                    style={{
                        margin: "42vh 0vh 0vh 8vh",
                        backgroundImage: "url('https://rotulosmatesanz.com/wp-content/uploads/2017/09/2000px-Google_G_Logo.svg_.png')"
                    }}
                ></a>
                <br />

                <a className="thirdLogin-login "
                    style={{
                        margin: "39vh 0vh 0vh 28vh",
                        backgroundImage: "url('https://upload.wikimedia.org/wikipedia/commons/5/51/Facebook_f_logo_%282019%29.svg')"
                    }}></a>
                <br />

                <a className="thirdLogin-login "
                    style={{
                        margin: "37vh 0vh 0vh 48vh",
                        backgroundImage: "url('https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Twitter-logo.svg/1200px-Twitter-logo.svg.png')"
                    }}></a>

            </div>
        </div>
    }
}



export default LoginIndex;


