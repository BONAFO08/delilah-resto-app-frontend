import React from "react";
import urlImgs from "./login.background.imgs.json"

import './login.index.css'
class Signup extends React.Component {

    state = {
        username: '',
        name: '',
        address: '',
        phone: '',
        email: '',
        password: '',

    }


    newUser = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        let requestOptions = {
            method: 'POST',
            body: JSON.stringify(dataInput),
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/signUp`, requestOptions)
            .then(response => response.json())
            .then(result => resp = result)
            .catch(error => console.log('error', error));
        return resp;
    }


    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        const validator =
            this.state.username.trim().length !== 0 &&
            this.state.name.trim().length !== 0 &&
            this.state.address.trim().length !== 0 &&
            this.state.phone.length !== 0 &&
            this.state.email.trim().length !== 0 &&
            this.state.password.trim().length !== 0;

        if (validator === true) {
            const response = this.newUser(this.state)
                .then(response => {
                    if(response.status === 200){
                        window.location.href = '/login'
                    }else{
                        document.getElementById('response-signup-h3').style.color = 'red';
                        document.getElementById('response-signup-h3').textContent = response.msj;
                    }
                })
                .catch(error => console.log(error));
        } else {
            document.getElementById('response-signup-h3').style.color = 'red';
            document.getElementById('response-signup-h3').textContent = 'HAY INFORMACION INVALIDA EN UNO DE LOS CAMPOS';
        }

    }

    render() {
        return <div className="background-login"
            style={{ backgroundImage: `url("${urlImgs.imgs[1]}")` }}>
            <div className="card-signup">
                <form onSubmit={this.onSubmit} method="POST">
                    <h3 id="title-login" className="h3-login"
                    >BIENVENIDO/A DELILAH FAMILY</h3>
                    <br />

                    <h3 id="username-signup-h3" className="h3-signup" style={{ margin: "0vh 0vh 0vh 14vh" }}
                    >NOMBRE DE USUARIO</h3>
                    <input required id="username-signup-input" className="input-login" type='text' name="username" value={this.state.username} onChange={this.onChange}
                        style={{ margin: "10vh 0vh 0vh 10vh" }}
                    />

                    <h3 id="name-signup-h3" className="h3-signup" style={{ margin: "0vh 0vh 0vh 28vh" }}
                    >NOMBRE Y APELLIDO</h3>
                    <input required id="name-signup-input" className="input-login" type='text' name="name" value={this.state.name} onChange={this.onChange}
                        style={{ margin: "0vh 0vh 0vh 24vh" }}
                    />


                    <br />
                    <h3 id="address-signup-h3" className="h3-signup" style={{ margin: "2vh 0vh 0vh 22vh" }}
                    >DOMICILIO</h3>
                    <input required id="address-signup-input" className="input-login" type='text' name="address" value={this.state.address} onChange={this.onChange}
                        style={{ margin: "10vh 0vh 0vh 10vh" }}
                    />

                    <h3 id="phone-signup-h3" className="h3-signup" style={{ margin: "2vh 0vh 0vh 38vh" }}
                    >TELEFONO</h3>
                    <input required id="phone-signup-input" className="input-login" type='number' name="phone" value={this.state.phone} onChange={this.onChange}
                        style={{ margin: "0vh 0vh 0vh 24vh" }}
                    />


                    <br />
                    <h3 id="email-signup-h3" className="h3-signup" style={{ margin: "2vh 0vh 0vh 12vh" }}
                    >CORREO ELECTRONICO</h3>
                    <input required id="email-signup-input" className="input-login" type='email' name="email" value={this.state.email} onChange={this.onChange}
                        style={{ margin: "10vh 0vh 0vh 10vh" }}
                    />

                    <h3 id="password-signup-h3" className="h3-signup" style={{ margin: "2vh 0vh 0vh 33vh" }}
                    >CONTRASEÑA</h3>
                    <input required id="password-signup-input" className="input-login" type='password' name="password" value={this.state.password} onChange={this.onChange}
                        style={{ margin: "0vh 0vh 0vh 24vh" }}
                    />

                    <h3 id="response-signup-h3" className="h3-signup" style={{ margin: "20vh 0vh 0vh -100vh" }}
                    ></h3>
                    <br />
                    <a className="button-login" id="btn-back-signup" href="/" style={{ margin: "12vh 0vh 0vh 10vh" }}
                    >VOLVER</a>
                    <input className="button-login" id="btn-send-signup" type='submit' value='ENVIAR' onSubmit={this.onSubmit} />
                    <a className="button-login" id="btn-login-signup" href="/login" style={{ margin: "12vh 0vh 0vh 50vh" }}
                    >YA TENGO CUENTA</a>
                </form>
            </div>
        </div>
    }
}



export default Signup;


