import React, { useState, useEffect } from "react";

function UserOrders(props) {

    const [orderData, setOrderData] = useState(props.ordersData);

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 58vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        "transition": "all 1s",
    })

    const [titleActualStyle, setTitleActualStyle] = useState({
        "fontSize": "5vh",
        "textAlign": "center",
        "color": "#000000",
        "margin": "3vh 0vh 0vh 0vh",
        "fontFamily": "'Montserrat', sans-serif",
        "cursor": "pointer",
        "color": "#000000"
    })

    const stadeColorActual = (stade) => {
        switch (stade) {
            case 'Confirmado':
                return "#1e8094"

            case 'En preparación':
                return "#86277b"

            case 'Enviado':
                return "#9cb006"

            case 'Entregado':
                return "#017e12"

            case 'Cancelado':
                return "#580909"
            default:
                return "#000000"

        }
    }

    const stadeColorHover = (stade) => {
        switch (stade) {
            case 'Confirmado':
                return "#2fc6e4"

            case 'En preparación':
                return "#cf3dbe"

            case 'Enviado':
                return "#cbe40b"

            case 'Entregado':
                return "#05ce20"

            case 'Cancelado':
                return "#e41717"
            default:
                return "#000000"

        }
    }

    const dataElementsBasicStyle = {
        "fontSize": "5vh",
        "textAlign": "center",
        "color": "#414394",
        "fontFamily": "'Montserrat', sans-serif",
        "transition": "all 0.5s",
        "margin": "4vh 0vh 0vh 0vh",
        "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000",
        "cursor": 'default'
    }


    const stadeBasicStyle = {
        "fontSize": "5vh",
        "textAlign": "center",
        "color": stadeColorActual(orderData.stade),
        "fontFamily": "'Montserrat', sans-serif",
        "transition": "all 0.5s",
        "margin": "4vh 0vh 0vh 0vh",
        "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000",
        "cursor": 'default'
    }


    const [formElementsTextActualStyle, setformElementsTextActualStyle] = useState({
        name: dataElementsBasicStyle,
        stade: stadeBasicStyle,
        phone: dataElementsBasicStyle,
        date: dataElementsBasicStyle,
        address: dataElementsBasicStyle,
        bill: dataElementsBasicStyle,
        dateInsideContainer: dataElementsBasicStyle,
        pay: dataElementsBasicStyle,
        cancelateButton: {
            "width": '40vh',
            "height": '10vh',
            "backgroundColor": "#4371ef",
            "border": "3px solid #000000",
            "borderRadius": "10%",
            "margin": "8vh 0vh 0vh 60vh",
            "fontSize": "35px",
            "display": 'inline',
            "transition": "all 0.5s"
        },
    })




    const [productButtonStyleDelete, setProductButtonStyleDelete] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "2vh 0vh 0vh 85vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute',
    })


    const [disableHover, setDisableHover] = useState(false)


    const onMouseEnterCard = () => {
        setContainerActualStyle({
            "width": "80vh",
            "height": "15vh",
            "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 58vh`,
            "border": "5px solid black ",
            "backgroundColor": "#2999c1",
            "borderRadius": "20%",
            "transition": "all 0.8s",
        })

        setTitleActualStyle(
            {
                "fontSize": "5vh",
                "textAlign": "center",
                "margin": "3vh 0vh 0vh 0vh",
                "fontFamily": "'Montserrat', sans-serif",
                "cursor": "pointer",
                "color": "#bdc4ff",
                "transition": "all 0.3s",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"

            }
        )

    }

    const onMouseLeaveCard = () => {
        setContainerActualStyle({
            "width": "80vh",
            "height": "15vh",
            "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 58vh`,
            "border": "5px solid black ",
            "backgroundColor": "#175065",
            "borderRadius": "20%",
            "transition": "all 1s",
        })

        setTitleActualStyle(
            {
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#000000",
                "margin": "3vh 0vh 0vh 0vh",
                "fontFamily": "'Montserrat', sans-serif",
                "cursor": "pointer",
                "color": "#000000",
            }
        )
    }

    const onMouseEnterFormElement = (e) => {
        const elementHTML = e.target.id.replace(orderData._id, '')
        if (elementHTML.includes('stade') === true) {
            setformElementsTextActualStyle(prevStyle => ({
                ...prevStyle,
                [elementHTML]: {
                    "fontSize": "7vh",
                    "textAlign": "center",
                    "color": stadeColorHover(orderData.stade),
                    "border": "5px solid #000000",
                    "backgroundColor": "transparent",
                    "borderRadius": "20%",
                    "fontFamily": "'Montserrat', sans-serif",
                    "transition": "all 0.5s",
                    "margin": "4vh 0vh 0vh 0vh",
                    "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                    "cursor": 'default',
                }
            }));
        } else {
            setformElementsTextActualStyle(prevStyle => ({
                ...prevStyle,
                [elementHTML]: {
                    "fontSize": "7vh",
                    "textAlign": "center",
                    "color": "#bdc4ff",
                    "border": "5px solid #000000",
                    "backgroundColor": "transparent",
                    "borderRadius": "20%",
                    "fontFamily": "'Montserrat', sans-serif",
                    "transition": "all 0.5s",
                    "margin": "4vh 0vh 0vh 0vh",
                    "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                    "cursor": 'default',

                }
            }));
        }
    }

    const onMouseLeaveFormElement = (e) => {
        const elementHTML = e.target.id.replace(orderData._id, '')

        if (elementHTML.includes('stade') === true) {
            setformElementsTextActualStyle(prevStyle => ({
                ...prevStyle,
                [elementHTML]: stadeBasicStyle

            }));
        } else {

            setformElementsTextActualStyle(prevStyle => ({
                ...prevStyle,
                [elementHTML]: dataElementsBasicStyle
            }));
        }
    }



    // const adaptBackground = () => {
    //     try {
    //         const background = document.getElementsByClassName('background-page');
    //         background[0].style.minHeight = `${250}vh`;
    //     } catch (error) {

    //     }
    // }

    const onMouseClickCard = () => {


        const container = document.getElementsByClassName('productContainer')



        if (!disableHover === true) {

            for (let i = 0; i < container.length; i++) {
                if (container[i].id === `${orderData._id}container`) {
                    for (let j = i + 1; j < container.length; j++) {
                        if (j == i + 1) {
                            container[j].style.margin = `${90 + (orderData.food.length * 85)}vh 0vh 0vh 58vh`;
                        } else {

                        }
                    }
                }
            }
            setContainerActualStyle({
                "width": "160vh",
                "height": `${(orderData.food.length === 1) ? (75 + (orderData.food.length * 80)) : (75 + (orderData.food.length * 60 - (orderData.food.length * 5)))}vh`,
                "margin": `${(props.index === 0) ? (10) : (40)}vh 0vh 0vh 18vh`,
                "border": "5px solid black ",
                "backgroundColor": "#2999c1",
                "borderRadius": "20%",
                "transition": "all 1s"
            })

            document.getElementById(`${orderData._id}dataContainer`).style.display = 'inline';
            setDisableHover(true)

            // adaptBackground()
        } else {
            for (let i = 0; i < container.length; i++) {
                if (container[i].id === `${orderData._id}container`) {
                    for (let j = i + 1; j < container.length; j++) {
                        container[j].style.margin = `20vh 0vh 0vh 58vh`;
                    }
                }
            }
            setContainerActualStyle({
                "width": "80vh",
                "height": "15vh",
                "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 58vh`,
                "border": "5px solid black ",
                "backgroundColor": "#2999c1",
                "borderRadius": "20%",
                "transition": "all 1s"
            })
            document.getElementById(`${orderData._id}dataContainer`).style.display = 'none';
            setDisableHover(false)
        }

    }


    const cancelateOrder = async (idOrder) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            redirect: 'follow',
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/cancelOrder/?idOrder=${idOrder}`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const dispacherUserOrder = (response) => {
        if (response.status === 200) {
            alert(response.msj)
            window.location.href = '/myOrders';
        }else{
            alert(response.msj)
        }
    }

    const fetchCancelateOrder = () => {
        const userConfirm = window.confirm(`¿ESTA SEGURO/A QUE QUIERE CANCELAR SU PEDIDO?
        \n ESTA ACCION SOLO PUEDE SER DESHECHA POR UN ADMINISTRADOR`);
        if (userConfirm === true) {
            cancelateOrder(orderData._id)
                .then(resolve => dispacherUserOrder(resolve))
                .catch(error => console.log(error))
        }
    }

    const showCancelateButton = () => {
        try {
            if (orderData.stade !== 'Entregado' && orderData.stade !== 'Cancelado') {

                return <button
                    style={formElementsTextActualStyle.cancelateButton}

                    onClick={
                        () => { fetchCancelateOrder() }
                    }
                    onMouseEnter={
                        () => {
                            setformElementsTextActualStyle(prevStyle => ({
                                ...prevStyle,
                                cancelateButton: {
                                    "width": '80vh',
                                    "height": '10vh',
                                    "backgroundColor": "#b01b1b",
                                    border: "3px solid #000000",
                                    borderRadius: "10%",
                                    margin: "8vh 0vh 0vh 40vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "all 0.5s"
                                }

                            }));
                        }
                    }
                    onMouseLeave={
                        () => {
                            setformElementsTextActualStyle(prevStyle => ({
                                ...prevStyle,
                                cancelateButton: {
                                    "width": '40vh',
                                    "height": '10vh',
                                    "backgroundColor": "#4371ef",
                                    border: "3px solid #000000",
                                    borderRadius: "10%",
                                    margin: "8vh 0vh 0vh 60vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "all 0.5s"
                                }

                            }));
                        }
                    }
                >CANCELAR PEDIDO
                </button>
            }
        } catch (error) {

        }
    }
   



  


    return <div style={{ backgroundColor: '#3c3c3c', width: '0vh', height: '0vh' }}>

        <div key={`${orderData._id}container`} id={`${orderData._id}container`} style={containerActualStyle}
            onMouseEnter={() => { if (!disableHover) { onMouseEnterCard() } }}
            onMouseLeave={() => { if (!disableHover) { onMouseLeaveCard() } }}
            className={"productContainer"}
        >

            <h3 style={titleActualStyle} onClick={() => { onMouseClickCard() }}>{orderData.date}</h3>




            <div style={{ display: 'none' }}
                id={`${orderData._id}dataContainer`} key={`${orderData._id}dataContainer`}>

                <h3 id={`${orderData._id}name`} className={'dataText'} key={`${orderData._id}name`}
                    style={formElementsTextActualStyle.name} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    NOMBRE :  {orderData.name}</h3>

                <h3 id={`${orderData._id}stade`} className={'dataText'} key={`${orderData._id}stade`}
                    style={formElementsTextActualStyle.stade} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    ESTADO :  {orderData.stade} </h3>

                <h3 id={`${orderData._id}phone`} className={'dataText'} key={`${orderData._id}phone`}
                    style={formElementsTextActualStyle.phone} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    TELEFONO :  {orderData.phone}</h3>

                <h3 id={`${orderData._id}date`} className={'dataText'} key={`${orderData._id}date`}
                    style={formElementsTextActualStyle.date} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    FECHA :  {orderData.date}</h3>

                <h3 id={`${orderData._id}address`} className={'dataText'} key={`${orderData._id}address`}
                    style={formElementsTextActualStyle.address} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    DOMICILIO :  {orderData.address}</h3>

                <h3 id={`${orderData._id}foodTitle`} key={`${orderData._id}foodTitle`} style={{
                    "fontSize": "5vh",
                    "textAlign": "center",
                    "color": "#414394",
                    "fontFamily": "'Montserrat', sans-serif",
                    "transition": "all 0.5s",
                    "margin": "4vh 0vh 0vh 0vh",
                    "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000",
                    "cursor": 'default'
                }}>COMIDA</h3>

                {props.food.map((food, index) => {





                    return <div id={`${orderData._id}foodContainer${index}`} key={`${orderData._id}foodContainer${index}`}>
                        <h3 style={{ backgroundColor: '#000000', padding: "0.5vh 0vh 0vh 20vh", borderRadius: '500%' }}></h3>
                        <h3 id={`${orderData._id}foodName${index}`} key={`${orderData._id}foodName${index}`}
                            style={{
                                "fontSize": "5vh",
                                "textAlign": "center",
                                "color": "#414394",
                                "fontFamily": "'Montserrat', sans-serif",
                                "transition": "all 0.5s",
                                "margin": "4vh 0vh 0vh 0vh",
                                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000",
                                "cursor": 'default'
                            }}
                            onMouseEnter={() => {
                                document.getElementById(`${orderData._id}foodName${index}`).style.fontSize = "7vh";
                                document.getElementById(`${orderData._id}foodName${index}`).style.textAlign = "center";
                                document.getElementById(`${orderData._id}foodName${index}`).style.color = "#bdc4ff";
                                document.getElementById(`${orderData._id}foodName${index}`).style.border = "5px solid #000000";
                                document.getElementById(`${orderData._id}foodName${index}`).style.backgroundColor = "transparent";
                                document.getElementById(`${orderData._id}foodName${index}`).style.borderRadius = "20%";
                                document.getElementById(`${orderData._id}foodName${index}`).style.fontFamily = "'Montserrat', sans-serif";
                                document.getElementById(`${orderData._id}foodName${index}`).style.transition = "all 0.5s";
                                document.getElementById(`${orderData._id}foodName${index}`).style.margin = "4vh 0vh 0vh 0vh";
                                document.getElementById(`${orderData._id}foodName${index}`).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";
                                document.getElementById(`${orderData._id}foodName${index}`).style.cursor = 'default';

                            }
                            } onMouseLeave={() => {
                                document.getElementById(`${orderData._id}foodName${index}`).style.fontSize = "5vh";
                                document.getElementById(`${orderData._id}foodName${index}`).style.textAlign = "center";
                                document.getElementById(`${orderData._id}foodName${index}`).style.color = "#414394";
                                document.getElementById(`${orderData._id}foodName${index}`).style.border = "";
                                document.getElementById(`${orderData._id}foodName${index}`).style.fontFamily = "'Montserrat', sans-serif";
                                document.getElementById(`${orderData._id}foodName${index}`).style.transition = "all 0.5s";
                                document.getElementById(`${orderData._id}foodName${index}`).style.margin = "4vh 0vh 0vh 0vh";
                                document.getElementById(`${orderData._id}foodName${index}`).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";
                                document.getElementById(`${orderData._id}foodName${index}`).style.cursor = 'default';
                            }
                            }
                        >PRODUCTO: {food.name}</h3>

                        <h3 id={`${orderData._id}foodAmmount${index}`} key={`${orderData._id}foodAmmount${index}`}
                            style={{
                                "fontSize": "5vh",
                                "textAlign": "center",
                                "color": "#414394",
                                "fontFamily": "'Montserrat', sans-serif",
                                "transition": "all 0.5s",
                                "margin": "4vh 0vh 0vh 0vh",
                                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000",
                                "cursor": 'default'
                            }}
                            onMouseEnter={() => {
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.fontSize = "7vh";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.textAlign = "center";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.color = "#bdc4ff";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.border = "5px solid #000000";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.backgroundColor = "transparent";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.borderRadius = "20%";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.fontFamily = "'Montserrat', sans-serif";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.transition = "all 0.5s";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.margin = "4vh 0vh 0vh 0vh";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.cursor = 'default';

                            }
                            } onMouseLeave={() => {
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.fontSize = "5vh";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.textAlign = "center";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.color = "#414394";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.border = "";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.fontFamily = "'Montserrat', sans-serif";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.transition = "all 0.5s";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.margin = "4vh 0vh 0vh 0vh";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";
                                document.getElementById(`${orderData._id}foodAmmount${index}`).style.cursor = 'default';
                            }
                            }
                        >CANTIDAD: {food.ammount}</h3>

                        <h3 id={`${orderData._id}foodPrice${index}`} key={`${orderData._id}foodPrice${index}`}
                            style={{
                                "fontSize": "5vh",
                                "textAlign": "center",
                                "color": "#414394",
                                "fontFamily": "'Montserrat', sans-serif",
                                "transition": "all 0.5s",
                                "margin": "4vh 0vh 0vh 0vh",
                                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000",
                                "cursor": 'default'
                            }}
                            onMouseEnter={() => {
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.fontSize = "7vh";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.textAlign = "center";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.color = "#bdc4ff";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.border = "5px solid #000000";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.backgroundColor = "transparent";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.borderRadius = "20%";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.fontFamily = "'Montserrat', sans-serif";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.transition = "all 0.5s";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.margin = "4vh 0vh 0vh 0vh";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.cursor = 'default';

                            }
                            } onMouseLeave={() => {
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.fontSize = "5vh";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.textAlign = "center";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.color = "#414394";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.border = "";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.fontFamily = "'Montserrat', sans-serif";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.transition = "all 0.5s";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.margin = "4vh 0vh 0vh 0vh";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";
                                document.getElementById(`${orderData._id}foodPrice${index}`).style.cursor = 'default';
                            }
                            }
                        >PRECIO UNITARIO: ${food.price}</h3>
                    </div>
                })}
                <h3 style={{ backgroundColor: '#000000', padding: "0.5vh 0vh 0vh 20vh", borderRadius: '500%' }}></h3>

                <h3 id={`${orderData._id}bill`} className={'dataText'} key={`${orderData._id}bill`}
                    style={formElementsTextActualStyle.bill} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    MONTO TOTAL :  ${orderData.bill}</h3>

                <h3 id={`${orderData._id}pay`} className={'dataText'} key={`${orderData._id}pay`}
                    style={formElementsTextActualStyle.pay} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    MEDIO DE PAGO :  {orderData.pay}</h3>

                <h3 id={`${props.food._id}dateInsideContainer`} className={'dataText'} key={`${props.food._id}dateInsideContainer`}
                    style={formElementsTextActualStyle.dateInsideContainer} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    FECHA :  {orderData.date}</h3>

                {showCancelateButton()}
            </div>



        </div>
    </div>
}


export default UserOrders;

