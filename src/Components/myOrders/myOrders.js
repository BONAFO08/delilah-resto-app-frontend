import React, { useState, useEffect } from "react";
import UserOrders from "./orders.myOrders";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";



function MyOrders(props) {

    const [finisher, setFinisher] = useState(0);
    const [dropdown, setDropdown] = useState(false);

    const [ordersData, setOrdersData] = useState([]);
    const [productData, setProductData] = useState([]);

    const [components, setComponents] = useState([]);

    const [showAllOrdersButtonActualStyle, setShowAllOrdersButtonActualStyle] = useState({
        "backgroundColor": '#316090',
        "padding": "1vh",
        "fontSize": "5vh",
        "color": "#316090",
        "border": "2px solid #3c72a9",
        "fontFamily": "'Montserrat', sans-serif",
        "transition": "all 0.5s",
        "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
        "display": "inline",
        "position": "absolute",
        "margin": "8vh 0vh 0vh 15vh",
        "borderRadius": '15%'
    })

    const [filterButtonActualStyle, setfilterButtonActualStyle] = useState({
        "backgroundColor": '#316090',
        "padding": "1vh",
        "fontSize": "5vh",
        width: '40vh',
        "color": "#316090",
        "border": "2px solid #3c72a9",
        "fontFamily": "'Montserrat', sans-serif",
        "transition": "all 0.5s",
        "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
        "display": "inline",
        "position": "absolute",
        "margin": "0vh 0vh 0vh 0vh",
        "borderRadius": '15%'
    })

    const openAndClose = () => {
        setDropdown(!dropdown);

    }


    const getMyOrders = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/showUserOrders`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const fetchOrderData = async () => {
        const responseOrders = await getMyOrders();
        const orders = await responseOrders.json();
        setOrdersData(orders)
    };


    const getProducts = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/products`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }




    const fetchProductData = async () => {
        const responseProducts = await getProducts();
        const products = await responseProducts.json();
        setProductData(products)
    };


 


    const dispatchAllOrders = (orders, filter) => {
        let ordersFiltred;
        if (filter !== undefined) {
            ordersFiltred = orders.filter(order => order.stade === filter);

        } else {
            ordersFiltred = orders;
        }
        if (ordersFiltred !== undefined) {
            const ordersToShow = ordersFiltred.map((order, index) => {
                const ordersData = order;
                const foodTemp = ordersData.food.map(food => {
                    if (food !== undefined && productData.length !== 0) {
                        const foodData = productData.filter(product => product._id === food.id);
                        if (foodData.length !== 0) {
                            return {
                                name: foodData[0].name,
                                price: foodData[0].price,
                                ammount: food.ammount
                            }
                        }
                    }
                }
                );
                return <UserOrders key={`${order._id}order`} index={index} ordersData={ordersData} food={foodTemp}></UserOrders>
            }
            )
            setComponents(ordersToShow)
        } else {
            return <div></div>
        }



    }


    const adaptBackground = () => {
        try {
            const background = document.getElementsByClassName('background-page');
            background[0].style.minHeight = `${100 + (ordersData.length * 14)}vh`;
        } catch (error) {

        }
    }





    useEffect(() => {
        props.validateUserToken()
        fetchOrderData()
        fetchProductData()

        setFinisher(1)

    }, [finisher]
    )


    return <div>
        <button onClick={() => { dispatchAllOrders(ordersData) }}
            onMouseEnter={() => {
                setShowAllOrdersButtonActualStyle({
                    "backgroundColor": '#4282c3',
                    "padding": "1vh",
                    "fontSize": "6vh",
                    "border": "5px solid #6c9ecf",
                    "color": "#8bc5ff",
                    "fontFamily": "'Montserrat', sans-serif",
                    "transition": "all 0.5s",
                    "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                    "display": "inline",
                    "position": "absolute",
                    "margin": "8vh 0vh 0vh 15vh",
                    "borderRadius": '15%'
                })
            }}
            onMouseLeave={() => {
                setShowAllOrdersButtonActualStyle({
                    "backgroundColor": '#316090',
                    "padding": "1vh",
                    "fontSize": "5vh",
                    "color": "#316090",
                    "border": "2px solid #3c72a9",
                    "fontFamily": "'Montserrat', sans-serif",
                    "transition": "all 0.5s",
                    "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                    "display": "inline",
                    "position": "absolute",
                    "margin": "8vh 0vh 0vh 15vh",
                    "borderRadius": '15%'
                })
            }}

            style={showAllOrdersButtonActualStyle}
        >MOSTRAR TODOS LOS PEDIDOS</button>

        <Dropdown isOpen={dropdown} toggle={openAndClose}
            style={{
                margin: "8vh 0vh 0vh 125vh",
                "display": "inline",
                "position": "absolute",
            }}>
            <DropdownToggle
                onMouseEnter={() => {
                    setfilterButtonActualStyle({
                        "backgroundColor": '#4282c3',
                        "padding": "1vh",
                        "fontSize": "6vh",
                        "width": '50vh',
                        "border": "5px solid #6c9ecf",
                        "color": "#8bc5ff",
                        "fontFamily": "'Montserrat', sans-serif",
                        "transition": "all 0.5s",
                        "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                        "display": "inline",
                        "position": "absolute",
                        "margin": "0vh 0vh 0vh 0vh",
                        "borderRadius": '15%'
                    })
                }}
                onMouseLeave={() => {
                    setfilterButtonActualStyle({
                        "backgroundColor": '#316090',
                        "padding": "1vh",
                        "fontSize": "5vh",
                        "width": '40vh',
                        "color": "#316090",
                        "border": "2px solid #3c72a9",
                        "fontFamily": "'Montserrat', sans-serif",
                        "transition": "all 0.5s",
                        "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                        "display": "inline",
                        "position": "absolute",
                        "margin": "0vh 0vh 0vh 0vh",
                        "borderRadius": '15%'
                    })
                }}

                style={filterButtonActualStyle}
            >
                FILTRAR POR...
            </DropdownToggle>
            <DropdownMenu style={{ backgroundColor: 'black' }} >

                <DropdownItem onClick={() => { dispatchAllOrders(ordersData, 'Confirmado') }} className="drop-item">
                    SOLO MOSTRAR CONFIRMADOS
                </DropdownItem>
                <DropdownItem onClick={() => { dispatchAllOrders(ordersData, 'En preparación') }} className="drop-item">
                    SOLO MOSTRAR EN PREPARACIÓN
                </DropdownItem>
                <DropdownItem onClick={() => { dispatchAllOrders(ordersData, 'Enviado') }} className="drop-item">
                    SOLO MOSTRAR ENVIADOS
                </DropdownItem>
                <DropdownItem onClick={() => { dispatchAllOrders(ordersData, 'Entregado') }} className="drop-item">
                    SOLO MOSTRAR ENTREGADOS
                </DropdownItem>
                <DropdownItem onClick={() => { dispatchAllOrders(ordersData, 'Cancelado') }} className="drop-item">
                    SOLO MOSTRAR CANCELADOS
                </DropdownItem>
            </DropdownMenu>
        </Dropdown>



        {/* 
        <Dropdown isOpen={dropdown} toggle={abrirCerrar} id='banner'>
                <DropdownToggle style={{ backgroundColor: 'transparent', }}>
                    <h3 id="imgDrop" style={{ backgroundImage: `url(${userPanelImg})` }}></h3>
                </DropdownToggle>

                <DropdownMenu style={{ backgroundColor: 'black' }} >
                    <BasicItems moveTo={props.moveTo}></BasicItems>
                    {showContent()}
                    <DropdownItem onClick={()=>{logOut()}} className="drop-item">
                        LOGOUT
                    </DropdownItem>
                </DropdownMenu>

            </Dropdown> */}

        <div id="spacer" style={{ padding: "25vh 0vh 0vh 0vh" }}></div>

        <h3 style={{ backgroundColor: '#000000', padding: "0.5vh 0vh 0vh 20vh", borderRadius: '500%' }}></h3>

        {components}
        {adaptBackground()}
    </div>
}


export default MyOrders;
