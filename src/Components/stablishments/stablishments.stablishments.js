import React from "react";
import Stablishment from "./stablishment";
import StablishmentData from "./stablishments.data.json"


function AllStablishments(props) {

    props.validateUserToken()
    return <div>
        {StablishmentData.stablishment.map(stablishment => <Stablishment key={`${stablishment.address}component`} data ={stablishment}></Stablishment>)}
    </div>
}



export default AllStablishments;


