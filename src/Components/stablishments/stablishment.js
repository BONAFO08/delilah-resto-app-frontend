import React, { useState } from "react";
import ComponentsStyles from '../../Styles/stablishments/allStablishments.style.json'

function Stablishment(props) {

    const [divActualStyle, setDivActualStyle] = useState(ComponentsStyles.div.actualStyle)
    const [stablishmentCityActualStyle, setStablishmentCityActualStyle] = useState(ComponentsStyles.stablishmentCity.actualStyle)
    const [stablishmentDataActualStyle, setStablishmentDataActualStyle] = useState(ComponentsStyles.stablishmentData.actualStyle)

    const onMouseEnter = () => {
        setDivActualStyle(ComponentsStyles.div.changeStyle)
        setStablishmentCityActualStyle(ComponentsStyles.stablishmentCity.changeStyle)
        setStablishmentDataActualStyle(ComponentsStyles.stablishmentData.changeStyle)
        document.getElementById(`${props.data.address}container`).style.backgroundColor = ComponentsStyles.div.changeStyle.backgroundColor;
    }

    const onMouseLeave = () => {
        setDivActualStyle(ComponentsStyles.div.actualStyle)
        setStablishmentCityActualStyle(ComponentsStyles.stablishmentCity.actualStyle)
        setStablishmentDataActualStyle(ComponentsStyles.stablishmentData.actualStyle)
        document.getElementById(`${props.data.address}container`).style.backgroundColor = '#000000';
    }


    return <div >
        <div id={`${props.data.address}container`} key={`${props.data.address}container`} style={divActualStyle} onMouseEnter={() => { onMouseEnter() }} onMouseLeave={() => { onMouseLeave() }}>

            <img key={`${props.data.address}img`} src={props.data.img}
                style={{ display: 'inline', width: '650px', height: '450px', borderRadius: '10%' }}></img>
            <h3 key={`${props.data.address}city`} style={stablishmentCityActualStyle}>{props.data.city.toUpperCase()}</h3>
            <p key={`${props.data.name}stablishmentData`} style={stablishmentDataActualStyle}>{props.data.address} <br></br><br></br> {props.data.hours} </p>

        </div>

    </div>
}






export default Stablishment;
