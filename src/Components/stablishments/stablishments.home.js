import React, { useState } from "react";
import '../../Styles/stablishments/stablishments.home.css'
import dataStablishments from './stablishments.data.json'

function StablishmentsHome(props) {
    const [stablishmentsImg, setStablishmentsImg] = useState(dataStablishments.stablishment);

    const randomizer = (max) => {
        return Math.floor(Math.random() * max)
    }


    const newElement = () => {
        return <div>
            <h3 className="h3ImgeStablishmentsHome" onClick={() => { props.moveTo('stablishments') }} >VISITANOS EN</h3>
            <img className="imgeStablishmentsHome" onClick={() => { props.moveTo('stablishments') }} src={`${stablishmentsImg[randomizer(stablishmentsImg.length)].img}`}></img>

        </div>
    }

    return <div>
        {/* <h3 id="perpo"></h3>
        <img id="imagen" ></img>
        {console.log(weatherData.length)}
        <Product productData={showOneProduct()}></Product>
        {weatherData.map(arr=><Product productData={showOneProduct()}></Product>)} */}

        {newElement()}

    </div>
}



export default StablishmentsHome;


