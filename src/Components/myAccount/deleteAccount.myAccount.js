import React, { useState, useEffect } from "react";


function DeleteAccount(props) {

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 68vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "background-color 0.5s",
        fontSize: "40px"
    })




    const fetchDeleteMyAccount = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow',
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/deleteUser`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const dispacherUserEliminated = (response) => {
        if (response.status === 200) {
            alert(`Adios, gracias por usar Delilah Resto. Hasta la proxima.`)
            window.sessionStorage.clear();
            window.location.href = '/';
        } else {
            alert(response.msj)
        }
    }



    const deleteMyAccount = () => {
        let userConfirm = prompt('¿Esta seguro/a de ELIMINAR su cuenta? \n PARA CONFIRMAR ESCRIBA "ELIMINAR"')
        if (userConfirm.trim() === "ELIMINAR") {
            userConfirm = window.confirm("¿Esta seguro/a ELIMINAR PERMANENTEMENTE su cuenta? \n ESTA ACCION NO SE PUEDE DESHACER");
            if (userConfirm === true) {
                fetchDeleteMyAccount()
                    .then(resolve => dispacherUserEliminated(resolve))
                    .catch(error => console.log(error))
            } else {
                alert('Borrado cancelado')
            }
        } else {
            alert('Borrado cancelado')
        }
    }




    return <div>
        {/* <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div> */}

        <button
            onClick={() => deleteMyAccount()}
            onMouseEnter={() => {
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 28vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#cf1d1d",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "50px"
                })
            }}

            onMouseLeave={() => {
                setContainerActualStyle({
                    "width": "80vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 68vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "40px"
                })
            }}
            style={containerActualStyle}
        >ELEMINAR MI CUENTA</button>
    </div>

}

export default DeleteAccount;

