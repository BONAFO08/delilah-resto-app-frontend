import React, { useState, useEffect } from "react";
import backgroundAdress from '../../Images/backgroundAdress.png';
import DeleteAccount from "./deleteAccount.myAccount";

function AddressBook(props) {

    const [addressBookButtonStyleCancel, setAddressBookButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "8vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [addressBookButtonStyleSend, setAddressBookButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "8vh 0vh 0vh 45vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [addressBookButtonStyleNew, setAddressBookButtonStyleNew] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "5vh 0vh 0vh 50vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });



    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 68vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "all 0.5s",
        fontSize: "40px"
    })




    const onMouseEnterButtonDeleteAddress = (e) => {
        document.getElementById(e.target.id).style.backgroundColor = '#ff0000';
        document.getElementById(e.target.id).style.transition = "all 0.5s";
        const boxContainerID = `${e.target.id.replace('buttonDelete', '')}boxContainer`;
        onMouseEnterFormElement({ target: { id: boxContainerID } })

    }

    const onMouseLeaveButtonDeleteAddress = (e) => {
        document.getElementById(e.target.id).style.backgroundColor = '#5c0f05d7';
        document.getElementById(e.target.id).style.transition = "all 0.5s";
        const boxContainerID = `${e.target.id.replace('buttonDelete', '')}boxContainer`;
        onMouseLeaveFormElement({ target: { id: boxContainerID } })

    }

    const onMouseEnterButtonDeleteChanges = (e) => {
        document.getElementById(e.target.id).style.backgroundColor = '#ff0000';
        document.getElementById(e.target.id).style.transition = "all 0.5s";
        const boxContainerID = `${e.target.id.replace('buttonCancelChanges', '')}boxContainer`;
        onMouseEnterFormElement({ target: { id: boxContainerID } })

    }

    const onMouseLeaveButtonDeleteChanges = (e) => {
        document.getElementById(e.target.id).style.backgroundColor = '#5c0f05d7';
        document.getElementById(e.target.id).style.transition = "all 0.5s";
        const boxContainerID = `${e.target.id.replace('buttonCancelChanges', '')}boxContainer`;
        onMouseLeaveFormElement({ target: { id: boxContainerID } })

    }

    

    const onMouseEnterFormElement = (e) => {
        const boxContainerID = (e.target.id.includes('boxContainer')) ? (e.target.id) : (`${e.target.id.replace('tag', '')}boxContainer`)

        document.getElementById(boxContainerID).style.border = '2px solid #000000';
        document.getElementById(boxContainerID).style.borderRadius = '10%';
        document.getElementById(boxContainerID).style.backgroundColor = '#11a7c858';


        const tagID = (e.target.id.includes('tag')) ? (e.target.id) : (`${e.target.id.replace('boxContainer', '')}tag`)

        document.getElementById(tagID).style.margin = '0% 2% 0% 20%';
        document.getElementById(tagID).style.fontSize = "50px";
        document.getElementById(tagID).style.color = "#414394";
        document.getElementById(tagID).style.cursor = "pointer";
        document.getElementById(tagID).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";



        document.getElementById(tagID).style.transition = "all 0.5s";
        document.getElementById(boxContainerID).style.transition = "all 0.5s";


    }



    const onMouseLeaveFormElement = (e) => {

        const boxContainerID = (e.target.id.includes('boxContainer')) ? (e.target.id) : (`${e.target.id.replace('tag', '')}boxContainer`)
        document.getElementById(boxContainerID).style.border = 'none';
        document.getElementById(boxContainerID).style.borderRadius = '10%';
        document.getElementById(boxContainerID).style.backgroundColor = 'transparent';

        const tagID = (e.target.id.includes('tag')) ? (e.target.id) : (`${e.target.id.replace('boxContainer', '')}tag`)


        document.getElementById(tagID).style.margin = '0% 2% 0% 20%';
        document.getElementById(tagID).style.fontSize = "40px";
        document.getElementById(tagID).style.color = "#bdc4ff";
        document.getElementById(tagID).style.textShadow = "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000";



        document.getElementById(tagID).style.transition = "all 0.5s";
        document.getElementById(boxContainerID).style.transition = "all 0.5s";



    }






    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }


    const dispacherUserBanned = (response, user) => {
        if (response.status === 200) {
            alert(`El usuario ${user} ha sido bloqueado exitosamente (Los cambios se reflejaran al reiniciar la sesion del usuario)`)
        } else {
            alert(`Lo siento pero el usuario ${user} no existe.`)
        }
    }


    const cancelate = () => {
        document.getElementById('adressBookContainer').style.display = 'none';
        document.getElementById('userBanName').value = "";


    }


    const showContainer = () => {
        document.getElementById('adressBookContainer').style.display = 'inline';
    }




    const showAddress = () => {
        const adressContainer = JSON.parse(window.sessionStorage.getItem('addressArr'));

        if (adressContainer !== "undefined" && adressContainer !== null) {


            const elements = adressContainer.map((element, index) => {
                return <div key={`address${index}container`}
                    id={`address${index}container`}>
                    <div id="spacer" style={{ padding: "2vh 0vh 0vh 0vh" }}></div>
                    <div id={`address${index}boxContainer`}
                        onMouseEnter={(e) => { onMouseEnterFormElement(e) }}
                        onMouseLeave={(e) => { onMouseLeaveFormElement(e) }} >
                        <h3
                            key={`address${index}tag`}
                            id={`address${index}tag`}
                            style={{
                                display: 'inline-block',
                                margin: '0% 2% 0% 20%',
                                fontSize: "40px",
                                color: '#bdc4ff',
                                textShadow: "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",

                            }}
                            onMouseEnter={(e) => { onMouseEnterFormElement(e) }}
                            onMouseLeave={(e) => { onMouseLeaveFormElement(e) }}
                            onDoubleClick={changeData}
                        >{element} </h3>

                        <input id={`address${index}input`} key={`address${index}input`} name={`address${index}`} type='text' className={'dataInput'}
                            style={{
                                display: 'none',
                                backgroundColor: "#18627c",
                                margin: '0% 2% 0% 20%',
                                borderRadius: "20%",
                                border: "3px solid #000000",
                                fontSize: "30px"
                            }}
                        ></input>

                        <button
                            key={`address${index}buttonDelete`}
                            id={`address${index}buttonDelete`}
                            onClick={(e) => {
                                deleteAddress(e);
                            }}
                            onMouseEnter={(e) => { onMouseEnterButtonDeleteAddress(e) }}
                            onMouseLeave={(e) => { onMouseLeaveButtonDeleteAddress(e) }}
                            style={{
                                padding: '0.5% 1% 0.5% 1%',
                                borderRadius: '50%',
                                backgroundColor: '#5c0f05d7',
                                color: '#ffffffc3',
                                textShadow: "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                                fontSize: "20px"
                            }}
                        >Eliminar</button>

                        <button
                            key={`address${index}buttonCancelChanges`}
                            id={`address${index}buttonCancelChanges`}
                            onClick={(e) => {
                                cancelateTHISchange(e);
                            }}
                            onMouseEnter={(e) => { onMouseEnterButtonDeleteChanges(e) }}
                            onMouseLeave={(e) => { onMouseLeaveButtonDeleteChanges(e) }}
                            style={{
                                display: 'none',
                                padding: '0.5% 1% 0.5% 1%',
                                borderRadius: '50%',
                                backgroundColor: '#5c0f05d7',
                                color: '#ffffffc3',
                                textShadow: "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000",
                                fontSize: "20px"
                            }}
                        >Cancelar</button>

                    </div>
                </div>
            })
            return elements
        }
    }




    const changeData = (e) => {
        document.getElementById(e.target.id).style.display = 'none';
        const inputID = `${e.target.id.replace('tag', '')}input`;
        document.getElementById(inputID).style.display = 'inline';


        const buttonDeleteID = `${e.target.id.replace('tag', '')}buttonDelete`;
        document.getElementById(buttonDeleteID).style.display = 'none';

        const buttonCancelChangesID = `${e.target.id.replace('tag', '')}buttonCancelChanges`;
        document.getElementById(buttonCancelChangesID).style.display = 'inline';
    }



    //CAMBIAR EL BACK!
    const fetchUpdateAddress = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/modAddress`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const updateAddress = async () => {
        const userConfirm = window.confirm('¿Guardar los cambios?');
        if (userConfirm === true) {
            const adressContainer = JSON.parse(window.sessionStorage.getItem('addressArr'));
            if (adressContainer !== "undefined" && adressContainer !== null) {
                const adressBook = [];
                for (let i = 0; i < adressContainer.length; i++) {
                    const newData = cleaningTheData(document.getElementById(`address${i}input`).value)
                    if (newData !== '' && newData !== adressContainer[i]) {
                        adressBook[i] = { address: newData, oldAdr: i + 1 };
                    } else {
                        adressBook[i] = null;
                    }
                }
                const validator = adressBook.filter(adress => adress !== null)
                if (validator.length !== 0) {
                    for (let i = 0; i < validator.length; i++) {
                        await fetchUpdateAddress(validator[i])
                    }
                    alert('Cambios guardados.')
                    window.location.href = '/myAccount';
                } else {
                    alert('No hay cambios que hacer...')
                }
            } else {
                alert('No hay cambios que hacer...')
            }
        }
    }

    const fetchDeleteAddress = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/delAddress`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const deleteAddress = async (e) => {
        let addressIndex = e.target.id;
        addressIndex = addressIndex.replace('address', '');
        addressIndex = addressIndex.replace('buttonDelete', '');
        addressIndex = parseInt(addressIndex);
        const adressContainer = JSON.parse(window.sessionStorage.getItem('addressArr'));

        const userConfirm = window.confirm(`¿Quiere ELIMINAR el domicilio ${adressContainer[addressIndex]} de su agenda?\n ESTA ACCION NO SE PUEDE DESHACER`);

        if (userConfirm === true) {
            await fetchDeleteAddress({ oldAdr: addressIndex +1 })
            alert(`${adressContainer[parseInt(addressIndex)]} eliminado de su agenda.`)
            const newAdressContainer = adressContainer.filter(address => address !== adressContainer[addressIndex]);
            window.sessionStorage.setItem('addressArr', JSON.stringify(newAdressContainer))
        }

    }


    const cancelateTHISchange = (e) => {
        let addressIndex = e.target.id;
        addressIndex = addressIndex.replace('address', '');
        addressIndex = addressIndex.replace('buttonCancelChanges', '');
        const adressContainer = JSON.parse(window.sessionStorage.getItem('addressArr'));
        const userConfirm = window.confirm(`¿Quiere descartar los cambios hechos en el domicilio ${adressContainer[parseInt(addressIndex)]}?`);
        if (userConfirm === true) {
            const tagID = `${e.target.id.replace('buttonCancelChanges', '')}tag`;
            document.getElementById(tagID).style.display = 'inline-block';
            const inputID = `${e.target.id.replace('buttonCancelChanges', '')}input`;
            document.getElementById(inputID).value = '';
            document.getElementById(inputID).style.display = 'none';
            const buttonDeleteID = `${e.target.id.replace('buttonCancelChanges', '')}buttonDelete`;
            document.getElementById(buttonDeleteID).style.display = 'inline';
            document.getElementById(e.target.id).style.display = 'none';

        }
    }

    const createAddress = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/newAddress`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const addNewAddress = () => {
        const newAddress = cleaningTheData(prompt('Escriba una nueva direccion.'))
        if (newAddress !== '') {
            const adressContainer = JSON.parse(window.sessionStorage.getItem('addressArr'));
            if (adressContainer !== "undefined" && adressContainer !== null) {
                const validator = adressContainer.filter(address => address === newAddress);
                if (validator.length === 0) {
                    adressContainer.push(newAddress);
                    window.sessionStorage.setItem('addressArr', JSON.stringify(adressContainer))
                    createAddress({ address: newAddress })
                        .then(resolve => console.log(resolve))
                        .catch(error => console.log(error))
                } else {
                    alert('Ese domicilio ya esta registrado en su agenda.')
                }
            }
        } else {
            alert('Error.Domicilio Invalido.')
        }
    }

    return <div>
        {/* <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div> */}

        < button
            onClick={() => showContainer()
            }
            onMouseEnter={() => {
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 28vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#2999c1",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "50px"
                })
            }}

            onMouseLeave={() => {
                setContainerActualStyle({
                    "width": "80vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 68vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "40px"
                })
            }}
            style={containerActualStyle}
        > AGENDA DE DIRECCIONES</button >

        <div
            id={"adressBookContainer"}
            style={{
                border: '0.3vh solid #000000',
                width: "150vh",
                height: `${40 + (((JSON.parse(window.sessionStorage.getItem('addressArr')) !== null) ? (JSON.parse(window.sessionStorage.getItem('addressArr')).length) : (0)) * 13)}vh`,
                backgroundImage: `url(${backgroundAdress})`,
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-20vh 0vh 0vh -120vh`,
            }}>




            <form onSubmit={(e) => { e.preventDefault(); }}>


                <h3
                    style={
                        {
                            textAlign: 'center',
                            fontSize: '50px'
                        }
                    }
                >AGENDA DE DOMICILIOS</h3>
                <br></br>
                {showAddress()}

                <button
                    onClick={() => addNewAddress()}
                    onMouseEnter={
                        () => {
                            setAddressBookButtonStyleNew({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "5vh 0vh 0vh 50vh",
                                fontSize: "35px",
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setAddressBookButtonStyleNew({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "5vh 0vh 0vh 50vh",
                                fontSize: "35px",
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={addressBookButtonStyleNew}
                >AGREGAR UNA DIRECCION</button>
                <br></br>
                {/* /////////////////////////////////////////////////// */}
                <button
                    onClick={() => updateAddress()}
                    onMouseEnter={
                        () => {
                            setAddressBookButtonStyleSend({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "8vh 0vh 0vh 45vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setAddressBookButtonStyleSend({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "8vh 0vh 0vh 45vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={addressBookButtonStyleSend}
                >GUARDAR CAMBIOS</button>

                <button
                    onClick={() => cancelate()}
                    onMouseEnter={
                        () => {
                            setAddressBookButtonStyleCancel({
                                "backgroundColor": "#b01b1b",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "8vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setAddressBookButtonStyleCancel({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "8vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={addressBookButtonStyleCancel}
                >CANCELAR</button>

            </form>

        </div>

    </div >

}

export default AddressBook;

