import React, { useState, useEffect } from "react";

import ComponentsStyles from '../../Styles/myAccount/myAccount.data.style.json'
import AddressBook from "./addressBook.myAccount.js";
import ChangePassword from "./changePassword.myAccount";
import DeleteAccount from "./deleteAccount.myAccount";


function MyAccount(props) {

    const [userData, setUserData] = useState({});
    const [oldData, setOldData] = useState({});

    const [finisher, setFinisher] = useState(0);

    const [myAccountButtonImgStyleSave, setMyAccountButtonImgStyleSave] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "13vh 0vh 0vh -70vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute'
    })


    const [myAccountStyleCancel, setMyAccountButtonImgStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "13vh 0vh 0vh -40vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute'
    })

    const [myAccountButtonStyleSend, setMyAccountButtonStyleSend] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 68vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "all 0.5s",
        fontSize: "40px"
    })


    const [formElementsTextActualStyle, setformElementsTextActualStyle] = useState({
        name: ComponentsStyles.formElementsText.actualStyle,
        username: ComponentsStyles.formElementsText.actualStyle,
        email: ComponentsStyles.formElementsText.actualStyle,
        phone: ComponentsStyles.formElementsText.actualStyle,
        typeAaccount: ComponentsStyles.formElementsText.actualStyle,
        userImg: {
            width: "45vh",
            height: "45vh",
            margin: '10vh 0vh 10vh 80vh',
            borderRadius: '50%',
            border: '2.5vh solid #272727',
            transition: 'border 1s',
            cursor: "pointer"
        }

    })

    const [formElementsInputActualStyle, setformElementsInputActualStyle] = useState({
        name: ComponentsStyles.formElementsInputs.actualStyle,
        username: ComponentsStyles.formElementsInputs.actualStyle,
        email: ComponentsStyles.formElementsInputs.actualStyle,
        phone: ComponentsStyles.formElementsInputs.actualStyle,
    })


    const changeImg = () => {
        document.getElementById(`${userData.name}containerImg`).style.display = 'inline';
    }

    const saveImg = (e) => {
        document.getElementById(`${userData.name}containerImg`).style.display = 'none';
        setOldData(prevStyle => ({
            ...prevStyle,
            userImg: userData.userImg
        }));
    }

    const cancelImg = (e) => {

        document.getElementById(`${userData.name}containerImg`).style.display = 'none';
        setUserData(prevStyle => ({
            ...prevStyle,
            userImg: oldData.userImg,
        }));

    }



    const nameTypeAccount = (typeAaccount) => {
        switch (typeAaccount) {
            case 0: return 'Local';
            case 1: return <span>
                <span style={{ color: '#4285f4' }}>G</span>
                <span style={{ color: '#ea4335' }}>O</span>
                <span style={{ color: '#fce59f' }}>O</span>
                <span style={{ color: '#4285f4' }}>G</span>
                <span style={{ color: '#34a853' }}>L</span>
                <span style={{ color: '#ea4335' }}>E</span>
            </span>;
            case 2: return 'Linkedin';
            case 3: return 'Facebook';
        }
    }


    const getUserData = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/showDataUser`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const fetchUserData = async () => {
        const responseUserData = await getUserData();
        const userData = await responseUserData.json();
        setUserData(userData)
        userData.imgUntouched = userData.userImg
        setOldData(userData)
        window.sessionStorage.setItem('addressArr', JSON.stringify(userData.address))
    };

    const updateUser = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/modUser`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }

    const validatingTheData = () => {
        const validator = { data: {}, boolean: false }



        if (userData.username.length === 0 || userData.username === oldData.username) {
            validator.data.username = oldData.username
            validator.boolean = validator.boolean || false
        } else {
            validator.data.username = userData.username
            validator.boolean = validator.boolean || true
        }

        if (userData.name.length === 0 || userData.name === oldData.name) {
            validator.data.name = oldData.name
            validator.boolean = validator.boolean || false
        } else {
            validator.data.name = userData.name
            validator.boolean = validator.boolean || true
        }

        if (userData.email.length === 0 || userData.email === oldData.email) {
            validator.data.email = oldData.email
            validator.boolean = validator.boolean || false
        } else {
            validator.data.email = userData.email
            validator.boolean = validator.boolean || true
        }


        if (userData.userImg.length === 0 || userData.userImg === oldData.imgUntouched) {
            validator.data.userImg = oldData.imgUntouched
            validator.boolean = validator.boolean || false
        } else {
            validator.data.userImg = userData.userImg
            validator.boolean = validator.boolean || true
        }

        if (isNaN(userData.phone) === true || userData.phone === oldData.phone) {
            validator.data.phone = oldData.phone
            validator.boolean = validator.boolean || false
        } else {
            validator.data.phone = userData.phone
            validator.boolean = validator.boolean || true
        }

        return validator
    }


    const dispacherUser = (response) => {
        if (response.status === 200) {
            window.sessionStorage.setItem('userImg', userData.userImg)
            window.location.href = '/myAccount';
        } else {
            alert(`Error. ${response.msj}`)
        }
    }


    const onSubmit = (e) => {

        userData.name = cleaningTheData(userData.name);
        userData.username = cleaningTheData(userData.username);
        userData.email = cleaningTheData(userData.email);
        userData.userImg = cleaningTheData(userData.userImg);

        const validator = validatingTheData();

        if (validator.boolean === true) {
            updateUser(validator.data)
                .then(resolve => dispacherUser(resolve))
                .catch(error => console.log(error))
        } else {
            alert('Error.Has introducido datos invalidos o no has cambiado nada.')
        }
    }

    const changeData = (e) => {
        e.target.style.display = 'none';
        document.getElementById(`${e.target.id}Input`).style.display = 'inline';
    }

    const onMouseEnterFormElement = (e) => {
        const elementHTML = e.target.id.replace(userData.name, '')
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            [elementHTML]: ComponentsStyles.formElementsText.onHover
        }));
    }

    const onMouseLeaveFormElement = (e) => {
        const elementHTML = e.target.id.replace(userData.name, '')
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            [elementHTML]: ComponentsStyles.formElementsText.actualStyle
        }));
    }

    const onMouseEnterImg = (e) => {
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            userImg: {
                width: "45vh",
                height: "45vh",
                margin: '10vh 0vh 10vh 80vh',
                borderRadius: '50%',
                border: '0.5vh solid #000000',
                transition: 'border 1s',
                cursor: "pointer"

            }
        }));
    }

    const onMouseLeaveImg = (e) => {
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            userImg: {
                width: "45vh",
                height: "45vh",
                margin: '10vh 0vh 10vh 80vh',
                borderRadius: '50%',
                border: '2.5vh solid #272727',
                transition: 'border 1s',
                cursor: "pointer"
            }
        }));
    }


    const onChange = (e) => {
        setUserData(prevStyle => ({
            ...prevStyle,
            [e.target.name]: e.target.value
        }));

    }



    useEffect(() => {
        props.validateUserToken()
        fetchUserData();
        setFinisher(1)
    }, [finisher]
    )


    return <div>
        <form onSubmit={(e) => { e.preventDefault() }}>
            <img
                style={formElementsTextActualStyle.userImg}
                onClick={changeImg}
                onMouseEnter={onMouseEnterImg}
                onMouseLeave={onMouseLeaveImg}
                src={userData.userImg}


            ></img>


            <h3 id={`${userData.name}username`} onDoubleClick={changeData} className={'dataText'}
                style={formElementsTextActualStyle.username} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
            >
                <span style={{ color: '#5f67dd', cursor: 'auto' }} >NOMBRE DE USUARIO :</span>   {userData.username}</h3>

            <input id={`${userData.name}usernameInput`} name={'username'} type='text' className={'dataInput'}
                style={formElementsInputActualStyle.username}
                onChange={onChange} value={userData.username}
            ></input>


            <h3 id={`${userData.name}name`} onDoubleClick={changeData} className={'dataText'}
                style={formElementsTextActualStyle.name} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
            >
                <span style={{ color: '#5f67dd', cursor: 'auto' }} >NOMBRE :</span>  {userData.name}</h3>


            <input id={`${userData.name}nameInput`} name={'name'} type='text' className={'dataInput'}
                style={formElementsInputActualStyle.name}
                onChange={onChange} value={userData.name}
            ></input>


            <h3 id={`${userData.name}email`} onDoubleClick={changeData} className={'dataText'}
                style={formElementsTextActualStyle.email} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
            >
                <span style={{ color: '#5f67dd', cursor: 'auto' }} >CORREO ELECTRONICO :</span>  {userData.email}</h3>

            <input id={`${userData.name}emailInput`} name={'email'} type='email' className={'dataInput'}
                style={formElementsInputActualStyle.email}
                onChange={onChange} value={userData.email}
            ></input>


            <h3 id={`${userData.name}phone`} onDoubleClick={changeData} className={'dataText'}
                style={formElementsTextActualStyle.phone} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
            >
                <span style={{ color: '#5f67dd', cursor: 'auto' }} >TELEFONO :</span>  {userData.phone}</h3>

            <input id={`${userData.name}phoneInput`} name={'phone'} type='number' className={'dataInput'}
                style={formElementsInputActualStyle.phone}
                onChange={onChange} value={userData.phone}
            ></input>


            <h3 id={`${userData.name}typeAaccount`} className={'dataText'}
                style={formElementsTextActualStyle.typeAaccount} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
            >
                <span style={{ color: '#5f67dd', cursor: 'auto' }} >TIPO DE CUENTA :</span>{nameTypeAccount(userData.typesAaccount)}</h3>




            <div id={`${userData.name}containerImg`} style={{
                border: '0.3vh solid #000000',
                width: "100vh",
                height: "80vh",
                backgroundColor: "#2999c1",
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-135vh 0vh 0vh 50vh`,
                zIndex: 300
            }}>

                <img src={userData.userImg} style={{
                    width: "50vh",
                    height: "50vh",
                    margin: "2vh 0vh 0vh 23vh",
                    border: '0.3vh solid #000000',
                }}></img>

                <input id={'typeInput'} name={'userImg'} type='text'
                    style={{
                        "display": "inline",
                        "fontSize": "5vh",
                        "textAlign": "center",
                        "margin": "1vh 0vh 0vh 2vh",
                        "width": '90vh',
                        "backgroundColor": "#2999c1",
                        "borderRadius": "10%",
                        "color": "#000000",
                        "fontFamily": "'Montserrat', sans-serif",
                        "cursor": "pointer",
                        "transition": "all 0.5s"
                    }
                    }
                    onChange={onChange} value={userData.userImg}></input>

                <button onClick={() => { saveImg() }}

                    style={
                        myAccountButtonImgStyleSave
                    }

                    onMouseEnter={() => {
                        setMyAccountButtonImgStyleSave({
                            "backgroundColor": "#15295f",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "13vh 0vh 0vh -70vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute'
                        })
                    }
                    }
                    onMouseLeave={() => {
                        setMyAccountButtonImgStyleSave({
                            "backgroundColor": "#4371ef",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "13vh 0vh 0vh -70vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute'
                        })
                    }
                    }

                >GUARDAR</button>

                <button onClick={() => { cancelImg() }}

                    style={
                        myAccountStyleCancel
                    }

                    onMouseEnter={() => {
                        setMyAccountButtonImgStyleCancel({
                            "backgroundColor": "#15295f",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "13vh 0vh 0vh -40vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute'
                        })
                    }
                    }
                    onMouseLeave={() => {
                        setMyAccountButtonImgStyleCancel({
                            "backgroundColor": "#4371ef",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "13vh 0vh 0vh -40vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute'
                        })
                    }
                    }

                >CANCELAR</button>
            </div>

        </form>

        <button onClick={() => { onSubmit() }}
            onMouseEnter={() => {
                setMyAccountButtonStyleSend({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 28vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#18bd55",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "50px"
                })
            }
            }
            onMouseLeave={() => {
                setMyAccountButtonStyleSend({
                    "width": "80vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 68vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "40px"
                })
            }
            }
            style={myAccountButtonStyleSend} >ENVIAR CAMBIOS</button>
        <AddressBook ></AddressBook>
        <ChangePassword typesAccount={userData.typesAaccount}></ChangePassword>
        <DeleteAccount></DeleteAccount>

    </div>
}


export default MyAccount;
