import React, { useState } from "react";


function ChangePassword(props) {

    const [changePasswordButtonStyleCancel, setChangePasswordButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 20vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [changePasswordButtonStyleSend, setChangePasswordButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 20vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 68vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "all 0.5s",
        fontSize: "40px"
    })


    const fetchChangePassword = async (data) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(data)
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/changePassword`, requestOptions)
            .then(response => resp = response.json())
            .catch(error => console.log('error', error));
        return resp;
    }

    const changePassword = () => {
        const oldPassword = cleaningTheData(document.getElementById('userOldPassword').value)
        const newPassword = cleaningTheData(document.getElementById('userNewPassword').value)
        const RepeatnewPassword = cleaningTheData(document.getElementById('userNewPasswordRepeat').value)


        if (oldPassword !== '' && newPassword !== '' && RepeatnewPassword !== '') {
            if (oldPassword !== newPassword) {
                if (newPassword === RepeatnewPassword) {
                    fetchChangePassword({ oldPassword: oldPassword, newPassword: newPassword })
                        .then(resolve => dispacherNewPassword(resolve))
                        .catch(error => console.log(error));
                } else {
                    alert('Ambas contraseñas nuevas deben ser iguales.')
                }
            } else {
                alert('La contaseña nueva es igual a la vieja.')
            }
        } else {
            alert('Lo siento has enviado datos invalidos.')
        }

    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }


    const dispacherNewPassword = (response) => {
        if (response.status === 200) {
            alert(`${response.msj}. Porfavor, vuelva a entrar a su cuenta.`)
            window.sessionStorage.clear()
            window.location.href = "/"

        } else {
            alert(response.msj)
        }
    }


    const cancelate = () => {
        document.getElementById('newPasswordContainer').style.display = 'none';
        document.getElementById('userOldPassword').value = "";
        document.getElementById('userNewPassword').value = "";
        document.getElementById('userNewPasswordRepeat').value = "";

    }


    const showContainer = () => {
        document.getElementById('newPasswordContainer').style.display = 'inline';
    }

    const showChangePasswordButton = () => {


        if (props.typesAccount === 0) {


            return <div>
                <button
                    id="changePasswordMainButton"
                    onClick={() => showContainer()}
                    onMouseEnter={() => {
                        setContainerActualStyle({
                            "width": "160vh",
                            "height": "15vh",
                            "margin": `10vh 0vh 0vh 28vh`,
                            "border": "5px solid black ",
                            "backgroundColor": "#2999c1",
                            "borderRadius": "20%",
                            transition: "all 0.5s",
                            fontSize: "50px"
                        })
                    }}

                    onMouseLeave={() => {
                        setContainerActualStyle({
                            "width": "80vh",
                            "height": "15vh",
                            "margin": `10vh 0vh 0vh 68vh`,
                            "border": "5px solid black ",
                            "backgroundColor": "#175065",
                            "borderRadius": "20%",
                            transition: "all 0.5s",
                            fontSize: "40px"
                        })
                    }}
                    style={containerActualStyle}
                >CAMBIAR CONTRASEÑA</button>

                <div
                    id={"newPasswordContainer"}
                    style={{
                        border: '0.3vh solid #000000',
                        width: "100vh",
                        height: "65vh",
                        backgroundColor: "#2999c1",
                        borderRadius: "20%",
                        position: 'absolute',
                        display: "none",
                        "margin": `-30vh 0vh 0vh -95vh`,
                    }}>

                    <br></br>
                    <form onSubmit={(e) => { e.preventDefault(); }}>

                        <h3
                            style={{
                                margin: "0% 23%",
                                fontSize: "40px"
                            }}
                        >ANTIGUA CONTRASEÑA </h3>


                        <input id={`userOldPassword`} name={'name'} type='password' className={'dataInput'}
                            placeholder="Vieja contraseña"


                            style={{
                                backgroundColor: "#18627c",
                                borderRadius: "20%",
                                border: "3px solid #000000",
                                margin: "2% 30%",
                                fontSize: "30px"
                            }}
                        ></input>

                        <h3
                            style={{
                                margin: "0% 26%",
                                fontSize: "40px"
                            }}
                        >NUEVA CONTRASEÑA </h3>


                        <input id={`userNewPassword`} name={'name'} type='password' className={'dataInput'}
                            placeholder="Nueva contraseña"


                            style={{
                                backgroundColor: "#18627c",
                                borderRadius: "20%",
                                border: "3px solid #000000",
                                margin: "2% 30%",
                                fontSize: "30px"
                            }}
                        ></input>




                        <h3
                            style={{
                                margin: "0% 18%",
                                fontSize: "40px"
                            }}
                        >REPITA LA NUEVA CONTRASEÑA </h3>


                        <input id={`userNewPasswordRepeat`} name={'name'} type='password' className={'dataInput'}
                            placeholder="Repita otra vez su nueva contraseña"


                            style={{
                                backgroundColor: "#18627c",
                                borderRadius: "20%",
                                border: "3px solid #000000",
                                margin: "2% 30%",
                                fontSize: "30px"
                            }}
                        ></input>

                        <button
                            onClick={() => changePassword()}
                            onMouseEnter={
                                () => {
                                    setChangePasswordButtonStyleSend({
                                        "backgroundColor": "#15295f",
                                        border: "3px solid #000000",
                                        borderRadius: "10%",
                                        margin: "2vh 0vh 0vh 20vh",
                                        fontSize: "35px",
                                        display: 'inline',
                                        transition: "background-color 0.5s"
                                    })
                                }
                            }
                            onMouseLeave={
                                () => {
                                    setChangePasswordButtonStyleSend({
                                        "backgroundColor": "#4371ef",
                                        border: "3px solid #000000",
                                        borderRadius: "10%",
                                        margin: "2vh 0vh 0vh 20vh",
                                        fontSize: "35px",
                                        display: 'inline',
                                        transition: "background-color 0.5s"
                                    })
                                }
                            }
                            style={changePasswordButtonStyleSend}
                        >CAMBIAR <br></br> CONTRASEÑA</button>

                        <button
                            onClick={() => cancelate()}
                            onMouseEnter={
                                () => {
                                    setChangePasswordButtonStyleCancel({
                                        "backgroundColor": "#15295f",
                                        border: "3px solid #000000",
                                        borderRadius: "10%",
                                        margin: "2vh 0vh 0vh 20vh",
                                        fontSize: "35px",
                                        display: 'inline',
                                        transition: "background-color 0.5s"
                                    })
                                }
                            }
                            onMouseLeave={
                                () => {
                                    setChangePasswordButtonStyleCancel({
                                        "backgroundColor": "#4371ef",
                                        border: "3px solid #000000",
                                        borderRadius: "10%",
                                        margin: "2vh 0vh 0vh 20vh",
                                        fontSize: "35px",
                                        display: 'inline',
                                        transition: "background-color 0.5s"
                                    })
                                }
                            }
                            style={changePasswordButtonStyleCancel}
                        >CANCELAR</button>

                    </form>

                </div>
            </div>


        }

    }



    return <div>
        {showChangePasswordButton()}
    </div>

}

export default ChangePassword;

