import React, { useState } from "react";
import ComponentsStyles from '../../Styles/products/allProducts.style.json'

function Product(props) {
    // productName               productNameActualStyle          setProductNameActualStyle
    // productIngredients           productIngredientsActualStyle         setProductIngredientsActualStyle

    const [divActualStyle, setDivActualStyle] = useState(ComponentsStyles.div.actualStyle)
    const [productNameActualStyle, setProductNameActualStyle] = useState(ComponentsStyles.productName.actualStyle)
    const [productIngredientsActualStyle, setProductIngredientsActualStyle] = useState(ComponentsStyles.productIngredients.actualStyle)
    const [productPriceActualStyle, setProductPriceActualStyle] = useState(ComponentsStyles.productPrice.actualStyle)

    const [shopCartButtonsActualStyle, setshopCartButtonsActualStyle] = useState(ComponentsStyles.shopCartButtons.actualStyle)

    const [shopCartTextActualStyle, setshopCartTextActualStyle] = useState(ComponentsStyles.shopCartTexts.actualStyle)


    const [wishList, setWishList] = useState(props.wishList)

    const onMouseEnter = () => {
        setDivActualStyle(ComponentsStyles.div.changeStyle)
        setProductNameActualStyle(ComponentsStyles.productName.changeStyle)
        setProductIngredientsActualStyle(ComponentsStyles.productIngredients.changeStyle)
        setProductPriceActualStyle(ComponentsStyles.productPrice.changeStyle)
        setshopCartButtonsActualStyle(ComponentsStyles.shopCartButtons.changeStyle)
        setshopCartTextActualStyle(ComponentsStyles.shopCartTexts.changeStyle)


        document.getElementById(`${props.data.name}container`).style.backgroundColor = ComponentsStyles.div.changeStyle.backgroundColor;

    }

    const onMouseLeave = () => {
        setDivActualStyle(ComponentsStyles.div.actualStyle)
        setProductNameActualStyle(ComponentsStyles.productName.actualStyle)
        setProductIngredientsActualStyle(ComponentsStyles.productIngredients.actualStyle)
        setProductPriceActualStyle(ComponentsStyles.productPrice.actualStyle)
        setshopCartButtonsActualStyle(ComponentsStyles.shopCartButtons.actualStyle)
        setshopCartTextActualStyle(ComponentsStyles.shopCartTexts.actualStyle)


        document.getElementById(`${props.data.name}container`).style.backgroundColor = '#000000';

    }

    const showContent = (product) => {
        if (wishList === undefined || wishList === null || wishList.filter(arr => arr._id === product._id).length === 0) {
            return <button   onMouseEnter={(e) => e.target.style.backgroundColor = '#444ede'} onMouseLeave={(e) => e.target.style.backgroundColor = '#bdc4ff'}
             id={`${product.name}addNewProductOption`} style={shopCartButtonsActualStyle} onClick={() => { props.addToCart(product) }}>AGREGAR AL CARRITO</button>
        } else {
            return <div>

                <h3 id={`${product.name}updateProductOptionAdd`} style={shopCartTextActualStyle}>YA ESTA EN EL CARRITO</h3>

            </div>

        }

    }


    const productIngredients = `${props.data.ingredients[0].toUpperCase()}${props.data.ingredients.substring(1)}`;

    return <div >

        <div id={`${props.data.name}container`} key={`${props.data.name}container`} style={divActualStyle} onMouseEnter={() => { onMouseEnter() }} onMouseLeave={() => { onMouseLeave() }}>

            <img key={`${props.data.name}img`} src={props.data.imgUrl}
                style={{ display: 'inline', width: '650px', height: '450px', borderRadius: '10%' }}></img>

            <h3 key={`${props.data.name}name`} style={productNameActualStyle}>{props.data.name.toUpperCase()}</h3>

            <p key={`${props.data.name}ingredients`} style={productIngredientsActualStyle}>{`INGREDIENTES:`}<br></br> {productIngredients} </p>
            
            <p key={`${props.data.name}price`} style={productPriceActualStyle}>{`PRECIO: $`}{props.data.price} </p>

            {showContent(props.data)}
        </div>

    </div>
}






export default Product;
