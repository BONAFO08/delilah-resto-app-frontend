import React, { useEffect, useState } from "react";
import Product from "./product";


function AllProducts(props) {
    const [productData, setProductData] = useState([]);
    const [finisher, setFinisher] = useState(0);



    const getProducts = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/products`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const newElements = () => {
        try {
            const elements = productData.map(data => <Product 
                addAmmountProduct={props.addAmmountProduct}
                reduceAmmountProduct={props.reduceAmmountProduct}
                deleteAmmountProduct={props.deleteAmmountProduct}
                addToCart={props.addToCart}
                wishList={props.wishList}
                key={data.name} 
                data={data}
            ></Product>);
            return elements
        } catch(error){
        }


    }

        const fetchProductData = async () => {
            const responseProducts = await getProducts();
            const products = await responseProducts.json();
            setProductData(products)
        };


    useEffect(() => {
        props.validateUserToken()
        fetchProductData()
        setFinisher(1)
    }, [finisher]
    )

    return <div>
        {newElements()}
    </div>
}



export default AllProducts;


