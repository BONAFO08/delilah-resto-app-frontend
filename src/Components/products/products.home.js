import React, { useEffect, useState } from "react";
import '../../Styles/products/product.home.css'


function PorductsHome(props) {
    const [productImgs, setProductImgs] = useState([]);
    const [finisher, setFinisher] = useState(0);

    const productURLDefault ='https://www.circuitogastronomico.com/wp-content/uploads/2021/04/panino-nota.jpg'; 

    const randomizer = (max) => {
        return Math.floor(Math.random() * max)
    }

    const getProducts = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/products`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const handleFetchProductData = () => {
        const fetchProductData = async () => {
            const responseProducts = await getProducts();
            const products = await responseProducts.json();
            const productsUrl = products.map(arr => arr.imgUrl)
            sessionStorage.setItem('productsImg', JSON.stringify(productsUrl))
        };
        if (sessionStorage.getItem('productsImg') === null) {
            fetchProductData();
            return JSON.parse(sessionStorage.getItem('productsImg'));
        } else {
            return JSON.parse(sessionStorage.getItem('productsImg'));
        }

    };

    const newElement = () => {
        try {
            return <div>
                <h3 className="h3ImgProductHome" onClick={() => { props.moveTo('products') }} >UNO DE MUCHOS</h3>
                <img className="imgProductHome" onClick={() => { props.moveTo('products') }} src={`${productImgs[randomizer(productImgs.length)]}`}></img>
            </div>
        } catch (error) {
            return <div>
                <h3 className="h3ImgProductHome" onClick={() => { props.moveTo('products') }} >UNO DE MUCHOS</h3>
                <img className="imgProductHome" onClick={() => { props.moveTo('products') }} src={`${productURLDefault}`}></img>
            </div>
        }
    }


    useEffect(() => {
        setProductImgs(handleFetchProductData())
        setFinisher(1)
    }, [finisher]
    );


    return <div>
        {newElement()}
    </div>
}



export default PorductsHome;


