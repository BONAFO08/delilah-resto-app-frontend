import React, { useState, useEffect } from "react";


function UnbanUser(props) {

    const [unbanButtonStyleCancel, setUnbanButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 12vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [unbanButtonStyleSend, setUnbanButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 12vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "160vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 18vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "background-color 0.5s",
        fontSize: "50px"
    })



    const fetchUnbanUser = async (data) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(data)
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/usersBan?ban=desbanear usuario`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const unbanUser = () => {
        const userData = cleaningTheData(document.getElementById('userUnbanName').value)
        if (userData !== '') {
            fetchUnbanUser({ name: userData })
                .then(resolve => dispacherUserUnbanned(resolve, userData))
                .catch();

        } else {
            alert('Lo siento has enviado datos invalidos.')
        }

    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }


    const dispacherUserUnbanned = (response, user) => {
        if (response.status === 200) {
            alert(`El usuario ${user} ha sido desbloqueado exitosamente (Los cambios se reflejaran al reiniciar la sesion del usuario)`)
        } else {
            alert(`Lo siento pero el usuario ${user} no existe.`)
        }
    }


    const cancelate = () => {
        document.getElementById('unbanContainer').style.display = 'none';
        document.getElementById('userUnbanName').value = "";


    }


    const showContainer = () => {
        document.getElementById('unbanContainer').style.display = 'inline';

    }


//2999c1

    return <div>
        

        <button
            onClick={() => showContainer()}
            onMouseEnter ={()=>{
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#2999c1",
                    "borderRadius": "20%",
                    transition: "background-color 0.5s",
                    fontSize: "50px"
                })
            }}

            onMouseLeave ={()=>{
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "background-color 0.5s",
                    fontSize: "50px"
                })
            }}
            style={containerActualStyle}
        >DESBLOQUEAR USUARIO</button>

        <div
            id={"unbanContainer"}
            style={{
                border: '0.3vh solid #000000',
                width: "80vh",
                height: "40vh",
                backgroundColor: "#2999c1",
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-20vh 0vh 0vh -120vh`,
            }}>


            <form onSubmit={(e) => { e.preventDefault(); }}>
                <h3
                    style={{
                        margin: "0% 18%",
                        fontSize: "40px"
                    }}
                > DESBLOQUEAR NOMBRE DE USUARIO O CORREO ELECTRONICO </h3>


                <input id={`userUnbanName`} name={'name'} type='text' className={'dataInput'}
                    placeholder="Usuario a desbloquear"


                    style={{
                        backgroundColor: "#18627c",
                        borderRadius: "20%",
                        border: "3px solid #000000",
                        margin: "2% 25%",
                        fontSize: "30px"
                    }}
                ></input>



                <button
                    onClick={() => unbanUser()}
                    onMouseEnter={
                        () => {
                            setUnbanButtonStyleSend({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 12vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setUnbanButtonStyleSend({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 12vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={unbanButtonStyleSend}
                >DESBLOQUEAR</button>

                <button
                    onClick={() => cancelate()}
                    onMouseEnter={
                        () => {
                            setUnbanButtonStyleCancel({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 12vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setUnbanButtonStyleCancel({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 12vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={unbanButtonStyleCancel}
                >CANCELAR</button>

            </form>

        </div>

    </div>

}

export default UnbanUser;

