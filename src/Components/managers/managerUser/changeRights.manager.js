import React, { useState, useEffect } from "react";


function ChangeRights(props) {

    const [changeRightsButtonStyleCancel, setChangeRightsButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "5vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [changeRightsButtonStyleSend, setChangeRightsButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "5vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "160vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 18vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "background-color 0.5s",
        fontSize: "50px"
    })

    const [labelClientActualStyle, setLabelClientActualStyle] = useState({
        margin: '0vh 0vh 0vh 15vh',
        position: 'absolute',
        display: 'inline',
        fontSize: "30px",
        color: '#253B67',
        textShadow: "0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000",
        transition: 'color 0.3s',
        cursor: "pointer"
    })

    const [labelAdminActualStyle, setLabelAdminActualStyle] = useState({
        margin: '0vh 0vh 0vh 50vh',
        position: 'absolute',
        display: 'inline',
        fontSize: "30px",
        color: '#253B67',
        textShadow: "0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000",
        transition: 'color 0.3s',
        cursor: "pointer"

    })


    const setRadioButton = (e) => {

        if (e.target.className.includes('client')) {

            document.getElementById(`userChangeRightsRightsClient`).checked = true
            document.getElementById(`userChangeRightsRightsAdmin`).checked = false
        } else {

            document.getElementById(`userChangeRightsRightsAdmin`).checked = true
            document.getElementById(`userChangeRightsRightsClient`).checked = false
        }
    }


    const fetchChangeRightsUser = async (data, rights) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(data)
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/rights?rights=${rights}`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const changeRights = () => {
        const userData = cleaningTheData(document.getElementById('userChangeRightsName').value)
        let userRights = '';

        if (document.getElementById(`userChangeRightsRightsClient`).checked === true) {
            userRights = 'client';
        } else if (document.getElementById(`userChangeRightsRightsAdmin`).checked === true) {
            userRights = 'admin';
        }

        if (userData !== '' && userRights !== '') {
            fetchChangeRightsUser({ name: userData }, userRights)
                .then(resolve => dispacherUserChanged(resolve, userData, userRights))
                .catch();
        } else {
            alert('Lo siento has enviado datos invalidos.')
        }

    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }


    const dispacherUserChanged = (response, user, rights) => {
        if (response.status === 200) {
            alert(`El usuario ${user} ahora es un ${rights} (Los cambios se reflejaran al reiniciar la sesion del usuario)`)
        } else {
            alert(`Lo siento pero el usuario ${user} no existe.`)
        }
    }


    const cancelate = () => {
        document.getElementById('changeRightsContainer').style.display = 'none';
        document.getElementById('userChangeRightsName').value = "";


    }


    const showContainer = () => {
        document.getElementById('changeRightsContainer').style.display = 'inline';

    }




    return <div>
        {/* <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div> */}

        <button
            onClick={() => showContainer()}
            onMouseEnter={() => {
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#2999c1",
                    "borderRadius": "20%",
                    transition: "background-color 0.5s",
                    fontSize: "50px"
                })
            }}

            onMouseLeave={() => {
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "background-color 0.5s",
                    fontSize: "50px"
                })
            }}
            style={containerActualStyle}
        >CAMBIAR PRIVILEGIOS</button>

        <div
            id={"changeRightsContainer"}
            style={{
                border: '0.3vh solid #000000',
                width: "80vh",
                height: "40vh",
                backgroundColor: "#2999c1",
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-10vh 0vh 0vh -120vh`,
            }}>


            <form onSubmit={(e) => { e.preventDefault(); }}>
                <h3
                    style={{
                        margin: "0% 18%",
                        fontSize: "40px"
                    }}
                >NOMBRE DE USUARIO O CORREO ELECTRONICO </h3>


                <input id={`userChangeRightsName`} name={'name'} type='text' className={'dataInput'}
                    placeholder="Cambiar privilegios de..."


                    style={{
                        backgroundColor: "#18627c",
                        borderRadius: "20%",
                        border: "3px solid #000000",
                        margin: "2% 25%",
                        fontSize: "30px"
                    }}
                ></input>
                <br></br>
                <input id={`userChangeRightsRightsClient`} name={"rights"} type={"radio"} value={'client'}
                    style={{
                        margin: '2vh 0vh 0vh 13vh',
                        position: 'absolute',
                        display: 'inline',
                    }}
                ></input>
                <label
                    className="client"
                    onClick={(e) => {
                        setRadioButton(e)
                    }}
                    style={labelClientActualStyle}
                    onMouseEnter={() => {
                        setLabelClientActualStyle({
                            margin: '0vh 0vh 0vh 15vh',
                            position: 'absolute',
                            display: 'inline',
                            fontSize: "30px",
                            color: '#5587EC',
                            textShadow: "0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000",
                            transition: 'color 0.3s',
                            cursor: "pointer"
                        })
                    }}

                    onMouseLeave={() => {
                        setLabelClientActualStyle({
                            margin: '0vh 0vh 0vh 15vh',
                            position: 'absolute',
                            display: 'inline',
                            fontSize: "30px",
                            color: '#253B67',
                            textShadow: "0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000",
                            transition: 'color 0.3s',
                            cursor: "pointer"
                        })
                    }}
                >CLIENTE</label>



                <input id={`userChangeRightsRightsAdmin`} name={"rights"} type={"radio"} value={'admin'}

                    style={{
                        margin: '2vh 0vh 0vh 47vh',
                        position: 'absolute',
                        display: 'inline',
                    }}
                ></input>

                <label
                    className="admin"
                    onClick={(e) => {
                        setRadioButton(e)
                    }}
                    style={labelAdminActualStyle}
                    onMouseEnter={() => {
                        setLabelAdminActualStyle({
                            margin: '0vh 0vh 0vh 50vh',
                            position: 'absolute',
                            display: 'inline',
                            fontSize: "30px",
                            color: '#5587EC',
                            textShadow: "0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000",
                            transition: 'color 0.3s',
                            cursor: "pointer"
                        })
                    }}

                    onMouseLeave={() => {
                        setLabelAdminActualStyle({
                            margin: '0vh 0vh 0vh 50vh',
                            position: 'absolute',
                            display: 'inline',
                            fontSize: "30px",
                            color: '#253B67',
                            textShadow: "0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000, 0px 0px 2px #000000",
                            transition: 'color 0.3s',
                            cursor: "pointer"
                        })
                    }}
                >ADMINISTRADOR</label>

                <br></br>
                <button
                    onClick={() => changeRights()}
                    onMouseEnter={
                        () => {
                            setChangeRightsButtonStyleSend({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "5vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setChangeRightsButtonStyleSend({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "5vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={changeRightsButtonStyleSend}
                >CAMBIAR<br></br>PRIVILEGIOS</button>

                <button
                    onClick={() => cancelate()}
                    onMouseEnter={
                        () => {
                            setChangeRightsButtonStyleCancel({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "5vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setChangeRightsButtonStyleCancel({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "5vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={changeRightsButtonStyleCancel}
                >CANCELAR</button>

            </form>

        </div>

    </div>

}

export default ChangeRights;

