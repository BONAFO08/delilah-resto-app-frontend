import React, { useState, useEffect } from "react";
import BanUser from "./banUser.manager";
import ChangeRights from "./changeRights.manager";
import AdminDeleteUser from "./deleteUser.manager";
import UnbanUser from "./unbanUser.manager";


function UserManager(props) {

    const [finisher, setFinisher] = useState(0);
    const [newPayButtonStyleCancel, setNewPayButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [newPayButtonStyleSend, setNewPayButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });



    // const getPays = async () => {
    //     let resp;
    //     let myHeaders = new Headers();
    //     myHeaders.append("Content-Type", "application/json");
    //     myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    //     let requestOptions = {
    //         method: 'GET',
    //         headers: myHeaders,
    //         redirect: 'follow'
    //     };

    //     await fetch(`${process.env.REACT_APP_URL_SERVER}/user/payments`, requestOptions)
    //         .then(response => resp = response)
    //         .catch(error => console.log('error', error));
    //     return resp;
    // }




    
    useEffect(() => {
        props.validateUserToken()
        setFinisher(1)
    }, [finisher]
    )


    return <div>

        <div id="spacer" style={{ padding: "20vh 0vh 0vh 0vh" }}></div>
        <BanUser></BanUser>
        <br></br>
        <UnbanUser></UnbanUser>
        <ChangeRights></ChangeRights>
        <AdminDeleteUser></AdminDeleteUser>
        <div id="spacer" style={{ padding: "20vh 0vh 0vh 0vh" }}></div>
    </div>
}






export default UserManager;


