import React, { useState, useEffect } from "react";


function BanUser(props) {

    const [newPayButtonStyleCancel, setNewPayButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [newPayButtonStyleSend, setNewPayButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "160vh",
        "height": "15vh",
        "margin": `0vh 0vh 0vh 18vh`,
        fontSize: "55px",
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        "transition": "all 1s"
    })


    // const getPays = async () => {
    //     let resp;
    //     let myHeaders = new Headers();
    //     myHeaders.append("Content-Type", "application/json");
    //     myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    //     let requestOptions = {
    //         method: 'GET',
    //         headers: myHeaders,
    //         redirect: 'follow'
    //     };

    //     await fetch(`${process.env.REACT_APP_URL_SERVER}/user/payments`, requestOptions)
    //         .then(response => resp = response)
    //         .catch(error => console.log('error', error));
    //     return resp;
    // }



    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }


    const dispacherPays = (response) => {
        if (response.status === 200) {
            window.location.href = '/managerPays';
        }
    }


    const cancelate = () => {
        document.getElementById('createNewPayContainer').style.display = 'none';

        document.getElementById('newPayName').value = "";
        document.getElementById('newPayImgUrl').value = "";


    }


    const showContainer = () => {
        document.getElementById('createNewPayContainer').style.display = 'inline';

    }




    return <div>
        {/* <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div> */}

        <button
            onClick={() => showContainer()}
            style={
                containerActualStyle

            }
        >BLOQUEAR USUARIO</button>

        <div
            id={"createNewPayContainer"}
            style={{
                border: '0.3vh solid #000000',
                width: "80vh",
                height: "60vh",
                backgroundColor: "#2999c1",
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-50vh 0vh 0vh -120vh`,
            }}>


            <form onSubmit={(e) => { e.preventDefault(); }}>
                <h3
                    style={{
                        margin: "0% 33%",
                        fontSize: "50px"
                    }}
                >NOMBRE</h3>


                <input id={`newPayName`} name={'name'} type='text' className={'dataInput'}
                    placeholder="Pago"


                    style={{
                        backgroundColor: "#18627c",
                        borderRadius: "20%",
                        border: "3px solid #000000",
                        margin: "0% 25%",
                        fontSize: "30px"
                    }}
                ></input>



                <h3
                    style={{
                        margin: "0% 28%",
                        fontSize: "50px"
                    }}
                >IMAGEN URL</h3>


                <input id={`newPayImgUrl`} name={'imgUrl'} type='text' className={'dataInput'}
                    placeholder="https://www.imagenEjemplo.com/"



                    style={{
                        backgroundColor: "#18627c",
                        borderRadius: "20%",
                        border: "3px solid #000000",
                        margin: "0% 25%",
                        fontSize: "30px"
                    }}
                ></input>

                <button
                    onClick={() => alert('estas baneado')}
                    onMouseEnter={
                        () => {
                            setNewPayButtonStyleSend({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setNewPayButtonStyleSend({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={newPayButtonStyleSend}
                >GUARDAR</button>

                <button
                    onClick={() => cancelate()}
                    onMouseEnter={
                        () => {
                            setNewPayButtonStyleCancel({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setNewPayButtonStyleCancel({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={newPayButtonStyleCancel}
                >CANCELAR</button>

            </form>

        </div>

    </div>

}

export default BanUser;

