import React, { useState, useEffect } from "react";


function AdminDeleteUser(props) {

    const [adminDeleteActualStyle, setAdminDeleteButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [adminDeleteButtonStyleSend, setAdminDeleteButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "160vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 18vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "background-color 0.5s",
        fontSize: "50px"
    })




    const fetchAdminDeleteUser = async (data) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(data)
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/adminDeleteUser`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const adminDeleteUser = () => {
        const userData = cleaningTheData(document.getElementById('userToDeleteName').value)
        const userConfirm = prompt(`SI ESTA SEGURO QUE QUIERE ELIMINAR AL USUARIO ${userData} \n POR FAVOR, ESCRIBA NUEVAMENTE SU NOMBRE PARA CONFIRMAR`)
        if (userData !== '' && userConfirm === userData) {
            fetchAdminDeleteUser({ name: userData })
                .then(resolve => dispacherUserDeleted(resolve, userData))
                .catch();

        } else {
            alert('Lo siento has enviado datos invalidos.')
        }

    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }


    const dispacherUserDeleted = (response, user) => {
        console.log(response.status);
        if (response.status === 200) {
            alert(`La cuenta del usuario ${user} ha sido eliminada exitosamente (Los cambios se reflejaran al reiniciar la sesion del usuario)`)
        } else {
            alert(`Lo siento pero el usuario ${user} no existe.`)
        }
    }


    const cancelate = () => {
        document.getElementById('admDeleteUserContainer').style.display = 'none';
        document.getElementById('userToDeleteName').value = "";


    }


    const showContainer = () => {
        document.getElementById('admDeleteUserContainer').style.display = 'inline';

    }




    return <div>
        {/* <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div> */}

        <button
            onClick={() => showContainer()}
            onMouseEnter={() => {
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#2999c1",
                    "borderRadius": "20%",
                    transition: "background-color 0.5s",
                    fontSize: "50px"
                })
            }}

            onMouseLeave={() => {
                setContainerActualStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "background-color 0.5s",
                    fontSize: "50px"
                })
            }}
            style={containerActualStyle}
        >ELIMINAR CUENTA DE UN USUARIO</button>

        <div
            id={"admDeleteUserContainer"}
            style={{
                border: '0.3vh solid #000000',
                width: "80vh",
                height: "40vh",
                backgroundColor: "#2999c1",
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-10vh 0vh 0vh -120vh`,
            }}>


            <form onSubmit={(e) => { e.preventDefault(); }}>
                <h3
                    style={{
                        margin: "0% 18%",
                        fontSize: "40px"
                    }}
                >NOMBRE DE USUARIO O CORREO ELECTRONICO A ELIMINAR </h3>


                <input id={`userToDeleteName`} name={'name'} type='text' className={'dataInput'}
                    placeholder="Cuenta a eliminar"


                    style={{
                        backgroundColor: "#18627c",
                        borderRadius: "20%",
                        border: "3px solid #000000",
                        margin: "2% 25%",
                        fontSize: "30px"
                    }}
                ></input>



                <button
                    onClick={() => adminDeleteUser()}
                    onMouseEnter={
                        () => {
                            setAdminDeleteButtonStyleSend({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setAdminDeleteButtonStyleSend({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={adminDeleteButtonStyleSend}
                >ELIMINAR <br></br> CUENTA</button>

                <button
                    onClick={() => cancelate()}
                    onMouseEnter={
                        () => {
                            setAdminDeleteButtonStyleCancel({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setAdminDeleteButtonStyleCancel({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "2vh 0vh 0vh 15vh",
                                fontSize: "35px",
                                display: 'inline',
                                transition: "background-color 0.5s"
                            })
                        }
                    }
                    style={adminDeleteActualStyle}
                >CANCELAR</button>

            </form>

        </div>

    </div>

}

export default AdminDeleteUser;

