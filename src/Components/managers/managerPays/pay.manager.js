import React, { useState, useEffect } from "react";
import PaysManagerData from "./pay.data.manager";

function PaysManager(props) {

    const [payData, setPayData] = useState([]);
    const [finisher, setFinisher] = useState(0);
    const [newPayButtonStyleCancel, setNewPayButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius : "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [newPayButtonStyleSend, setNewPayButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius : "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });






    const getPays = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/payments`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const newElements = () => {
        try {

   
            const elements = payData.map((data, index) =>
                <div>
                    <PaysManagerData
                        key={data.name}
                        payData={data}
                        index={index}
                    ></PaysManagerData>

                </div>
            );
            return elements
        } catch (error) {
        }
    }

    const fetchPayData = async () => {
        const responsePays = await getPays();
        const pays = await responsePays.json();
        setPayData(pays)
    };


    const adaptBackground = () => {
        const background = document.getElementsByClassName('background-page');
        background[0].style.minHeight = `${100 + (payData.length * 14)}vh`;
    }




    const validatingTheData = (dataValue) => {
        const validator = { boolean: true }

        if (dataValue.name.length === 0) {
            
            validator.boolean = validator.boolean && false
        } else {
            validator.boolean = validator.boolean && true
        }

        if (dataValue.imgUrl.length === 0) {
            
            validator.boolean = validator.boolean && false
        } else {
            validator.boolean = validator.boolean && true
        }

        return validator

    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }

    const createPay = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/newPay` , requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const dispacherPays = (response) => {
        if (response.status === 200) {
            window.location.href = '/managerPays';
        }
    }


    const sendNewPayData = () => {
        const newPayData = {
            name: document.getElementById('newPayName').value,
            imgUrl: document.getElementById('newPayImgUrl').value,
        }


        newPayData.name = cleaningTheData(newPayData.name);
        newPayData.imgUrl = cleaningTheData(newPayData.imgUrl);

        const validator = validatingTheData(newPayData);

        if (validator.boolean === true) {
            createPay(newPayData)
                .then(resolve => dispacherPays(resolve))
                .catch(error => console.log(error))
        } else {
            alert('Error.Uno o mas de los datos proporcionados son invalidos.')
        }


    }

    const cancelate = () => {
        document.getElementById('createNewPayContainer').style.display = 'none';

        document.getElementById('newPayName').value = "";
        document.getElementById('newPayImgUrl').value = "";


    }


    const showContainer = () => {
        document.getElementById('createNewPayContainer').style.display = 'inline';

    }
    useEffect(() => {
        props.validateUserToken()
        fetchPayData()
        adaptBackground();
        setFinisher(1)
    }, [finisher]
    )


    return <div>
        {newElements()}
        <div>
            <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div>

            <button
                onClick={() => showContainer()}
                style={{
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `0vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    "transition": "all 1s",
                    fontSize: "50px"

                }}
            >CREAR NUEVO METODO DE PAGO</button>

            <div
                id={"createNewPayContainer"}
                style={{
                    border: '0.3vh solid #000000',
                    width: "80vh",
                    height: "60vh",
                    backgroundColor: "#2999c1",
                    borderRadius: "20%",
                    position: 'absolute',
                    display: "none",
                    "margin": `-50vh 0vh 0vh -120vh`,
                }}>


                <form onSubmit={(e) => { e.preventDefault(); }}>
                    <h3
                        style={{
                            margin: "0% 33%",
                            fontSize: "50px"
                        }}
                    >NOMBRE</h3>


                    <input id={`newPayName`} name={'name'} type='text' className={'dataInput'}
                        placeholder="Pago"


                        style={{
                            backgroundColor : "#18627c",
                            borderRadius: "20%",
                            border: "3px solid #000000",
                            margin: "0% 25%",
                            fontSize: "30px"
                        }}
                    ></input>



                    <h3
                        style={{
                            margin: "0% 28%",
                            fontSize: "50px"
                        }}
                    >IMAGEN URL</h3>


                    <input id={`newPayImgUrl`} name={'imgUrl'} type='text' className={'dataInput'}
                        placeholder="https://www.imagenEjemplo.com/"

    

                        style={{
                            backgroundColor : "#18627c",
                            borderRadius: "20%",
                            border: "3px solid #000000",
                            margin: "0% 25%",
                            fontSize: "30px"
                        }}
                    ></input>

                    <button
                        onClick={() => sendNewPayData()}
                        onMouseEnter ={
                            ()=>{
                                setNewPayButtonStyleSend({
                                    "backgroundColor": "#15295f",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        onMouseLeave ={
                            ()=>{
                                setNewPayButtonStyleSend({
                                    "backgroundColor": "#4371ef",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        style={newPayButtonStyleSend}
                    >GUARDAR</button>

                    <button
                        onClick={() => cancelate()}
                        onMouseEnter ={
                            ()=>{
                                setNewPayButtonStyleCancel({
                                    "backgroundColor": "#15295f",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        onMouseLeave ={
                            ()=>{
                                setNewPayButtonStyleCancel({
                                    "backgroundColor": "#4371ef",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        style={newPayButtonStyleCancel}
                    >CANCELAR</button>

                </form>

            </div>

        </div>
    </div>
}






export default PaysManager;


