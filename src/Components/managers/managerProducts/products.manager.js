import React, { useState, useEffect } from "react";
import ProductsManagerData from "./products.data.manager";

function ProductsManager(props) {

    const [productData, setProductData] = useState([]);
    const [finisher, setFinisher] = useState(0);
    const [newProductButtonStyleCancel, setNewProductButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius : "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });

    const [newProductButtonStyleSend, setNewProductButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius : "10%",
        margin: "2vh 0vh 0vh 15vh",
        fontSize: "35px",
        display: 'inline',
        transition: "background-color 0.5s"
    });



    // const [newProductData, setNewProductData] = useState({})



    const getProducts = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/products`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const newElements = () => {
        try {

   
            const elements = productData.map((data, index) =>
                <div>
                    <ProductsManagerData
                        key={data.name}
                        productData={data}
                        index={index}
                    ></ProductsManagerData>

                </div>
            );
            return elements
        } catch (error) {
        }
    }

    const fetchProductData = async () => {
        const responseProducts = await getProducts();
        const products = await responseProducts.json();
        setProductData(products)
    };


    const adaptBackground = () => {
        const background = document.getElementsByClassName('background-page');
        background[0].style.minHeight = `${100 + (productData.length * 14)}vh`;
    }


    // const onChange = (e) => {
    //     setNewProductData(prevStyle => ({
    //         ...prevStyle,
    //         [e.target.name]: e.target.value
    //     }));

    // }


    const validatingTheData = (dataValue) => {
        const validator = { boolean: true }




        if (dataValue.name.length === 0) {
            
            validator.boolean = validator.boolean && false
        } else {
            validator.boolean = validator.boolean && true
        }

        if (isNaN(dataValue.price) === true) {
            
            validator.boolean = validator.boolean && false
        } else {
            validator.boolean = validator.boolean && true
        }

        if (dataValue.ingredients.length === 0) {
            
            validator.boolean = validator.boolean && false
        } else {
            validator.boolean = validator.boolean && true
        }

        if (dataValue.imgUrl.length === 0) {
            
            validator.boolean = validator.boolean && false
        } else {
            validator.boolean = validator.boolean && true
        }

        return validator

    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }

    const createProduct = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/newProduct` , requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const dispacherProducts = (response) => {
        if (response.status === 200) {
            window.location.href = '/managerProducts';
        }
    }


    const sendNewProductData = () => {
        const newProductData = {
            name: document.getElementById('newProductName').value,
            ingredients: document.getElementById('newProductIngredients').value,
            imgUrl: document.getElementById('newProductImgUrl').value,
            price: document.getElementById('newProductPrice').value
        }


        newProductData.name = cleaningTheData(newProductData.name);
        newProductData.ingredients = cleaningTheData(newProductData.ingredients);
        newProductData.imgUrl = cleaningTheData(newProductData.imgUrl);
        newProductData.price = parseInt(newProductData.price);

        const validator = validatingTheData(newProductData);

        if (validator.boolean === true) {
            createProduct(newProductData)
                .then(resolve => dispacherProducts(resolve))
                .catch(error => console.log(error))
        } else {
            alert('Error.Uno o mas de los datos proporcionados son invalidos.')
        }


    }

    const cancelate = () => {
        document.getElementById('createNewProductContainer').style.display = 'none';

        document.getElementById('newProductName').value = "";
        document.getElementById('newProductIngredients').value = "";
        document.getElementById('newProductPrice').value = "";
        document.getElementById('newProductImgUrl').value = "";


    }


    const showContainer = () => {
        document.getElementById('createNewProductContainer').style.display = 'inline';

    }
    useEffect(() => {
        props.validateUserToken()
        fetchProductData()
        adaptBackground();
        setFinisher(1)
    }, [finisher]
    )


    return <div>
        {newElements()}
        <div>
            <div id="spacer" style={{padding:"80vh 0vh 0vh 0vh"}}></div>

            <button
                onClick={() => showContainer()}
                style={{
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `0vh 0vh 0vh 18vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    "transition": "all 1s",
                    fontSize: "50px"

                }}
            >CREAR NUEVO PRODUCTO</button>

            <div
                id={"createNewProductContainer"}
                style={{
                    border: '0.3vh solid #000000',
                    width: "80vh",
                    height: "60vh",
                    backgroundColor: "#2999c1",
                    borderRadius: "20%",
                    position: 'absolute',
                    display: "none",
                    "margin": `-50vh 0vh 0vh -120vh`,
                }}>


                <form onSubmit={(e) => { e.preventDefault(); }}>
                    <h3
                        style={{
                            margin: "0% 33%",
                            fontSize: "50px"
                        }}
                    >NOMBRE</h3>


                    <input id={`newProductName`} name={'name'} type='text' className={'dataInput'}
                        placeholder="Comida"

                        // onChange={onChange} value={newProductData.name}

                        style={{
                            backgroundColor : "#18627c",
                            borderRadius: "20%",
                            border: "3px solid #000000",
                            margin: "0% 25%",
                            fontSize: "30px"
                        }}
                    ></input>

                    <h3
                        style={{
                            margin: "0% 25%",
                            fontSize: "50px"
                        }}
                    >INGREDIENTES</h3>


                    <input id={`newProductIngredients`} name={'ingredients'} type='text' className={'dataInput'}
                        placeholder="ingrediente 1, ingrediente 2..."

                        // onChange={onChange} value={newProductData.ingredients}

                        style={{
                            backgroundColor : "#18627c",
                            borderRadius: "20%",
                            border: "3px solid #000000",
                            margin: "0% 25%",
                            fontSize: "30px"
                        }}
                    ></input>


                    <h3
                        style={{
                            margin: "0% 33%",
                            fontSize: "50px"
                        }}
                    >PRECIO</h3>


                    <input id={`newProductPrice`} name={'price'} type='number' className={'dataInput'}
                        placeholder="0"

                        // onChange={onChange} value={newProductData.price}


                        style={{
                            backgroundColor : "#18627c",
                            borderRadius: "20%",
                            border: "3px solid #000000",
                            margin: "0% 25%",
                            fontSize: "30px"
                        }}
                    ></input>


                    <h3
                        style={{
                            margin: "0% 28%",
                            fontSize: "50px"
                        }}
                    >IMAGEN URL</h3>


                    <input id={`newProductImgUrl`} name={'imgUrl'} type='text' className={'dataInput'}
                        placeholder="https://www.imagenEjemplo.com/"

                        // onChange={onChange} value={newProductData.imgUrl}

                        style={{
                            backgroundColor : "#18627c",
                            borderRadius: "20%",
                            border: "3px solid #000000",
                            margin: "0% 25%",
                            fontSize: "30px"
                        }}
                    ></input>

                    <button
                        onClick={() => sendNewProductData()}
                        onMouseEnter ={
                            ()=>{
                                setNewProductButtonStyleSend({
                                    "backgroundColor": "#15295f",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        onMouseLeave ={
                            ()=>{
                                setNewProductButtonStyleSend({
                                    "backgroundColor": "#4371ef",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        style={newProductButtonStyleSend}
                    >GUARDAR</button>

                    <button
                        onClick={() => cancelate()}
                        onMouseEnter ={
                            ()=>{
                                setNewProductButtonStyleCancel({
                                    "backgroundColor": "#15295f",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        onMouseLeave ={
                            ()=>{
                                setNewProductButtonStyleCancel({
                                    "backgroundColor": "#4371ef",
                                    border: "3px solid #000000",
                                    borderRadius : "10%",
                                    margin: "2vh 0vh 0vh 15vh",
                                    fontSize: "35px",
                                    display: 'inline',
                                    transition: "background-color 0.5s"
                                })
                            }
                        }
                        style={newProductButtonStyleCancel}
                    >CANCELAR</button>

                </form>

            </div>

        </div>
    </div>
}






export default ProductsManager;


