import React, { useState } from "react";
import ComponentsStyles from '../../../Styles/managers/products.manager.style.json'
import SeparatorImg from '../../../Images/separator.png'

function ProductsManagerData(props) {
    const [containerActualStyle, setContainerActualStyle] = useState({
        "width": "160vh",
        "height": "15vh",
        "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 18vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        "transition": "all 1s"
    })

    const [titleActualStyle, setTitleActualStyle] = useState(ComponentsStyles.title.actualStyle)

    const [formElementsTextActualStyle, setformElementsTextActualStyle] = useState({
        name: ComponentsStyles.formElementsText.actualStyle,
        ingredients: ComponentsStyles.formElementsText.actualStyle,
        img: ComponentsStyles.formElementsText.actualStyle,
        price: ComponentsStyles.formElementsText.actualStyle,
    })

    const [formElementsInputActualStyle, setformElementsInputActualStyle] = useState({
        name: ComponentsStyles.formElementsInputs.actualStyle,
        ingredients: ComponentsStyles.formElementsInputs.actualStyle,
        img: ComponentsStyles.formElementsInputs.actualStyle,
        price: ComponentsStyles.formElementsInputs.actualStyle,
    })

    const [productButtonStyleSend, setProductButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "2vh 0vh 0vh 55vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute',
    })
    const [productButtonStyleDelete, setProductButtonStyleDelete] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "2vh 0vh 0vh 85vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute',
    })

    const [productButtonImgStyleSave, setProductButtonImgStyleSave] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "13vh 0vh 0vh -70vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute'
    })


    const [productButtonImgStyleCancel, setProductButtonImgStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        border: "3px solid #000000",
        borderRadius: "10%",
        transition: "background-color 0.5s",
        margin: "13vh 0vh 0vh -40vh",
        fontSize: "40px",
        display: 'inline',
        position: 'absolute'
    })





    const [dataValue, setDataValue] = useState(props.productData)
    const [oldData, setOldData] = useState(props.productData)



    const [disableHover, setDisableHover] = useState(false)


    const onMouseEnterCard = () => {
        setContainerActualStyle({
            "width": "160vh",
            "height": "15vh",
            "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 18vh`,
            "border": "5px solid black ",
            "backgroundColor": "#2999c1",
            "borderRadius": "20%",
            "transition": "background-color 0.8s"
        })

    }

    const onMouseLeaveCard = () => {
        setContainerActualStyle({
            "width": "160vh",
            "height": "15vh",
            "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 18vh`,
            "border": "5px solid black ",
            "backgroundColor": "#175065",
            "borderRadius": "20%",
            "transition": "all 1s"
        })
    }

    const onMouseClickCard = () => {


        const cosa = document.getElementsByClassName('productContainer')
        // const thisElement = cosa.filter(arr => arr.id === `${props.productData.name}container`)



        if (!disableHover) {

            for (let i = 0; i < cosa.length; i++) {
                if (cosa[i].id === `${props.productData.name}container`) {
                    for (let j = i + 1; j < cosa.length; j++) {
                        cosa[j].style.margin = `90vh 0vh 0vh 18vh`;
                    }
                }
            }
            setContainerActualStyle({
                "width": "160vh",
                "height": "75vh",
                "margin": `${(props.index === 0) ? (10) : (40)}vh 0vh 0vh 18vh`,
                "border": "5px solid black ",
                "backgroundColor": "#2999c1",
                "borderRadius": "20%",
                "transition": "all 1s"
            })

            document.getElementById(`${props.productData.name}formData`).style.display = 'inline';
            setDisableHover(true)
        } else {
            for (let i = 0; i < cosa.length; i++) {
                if (cosa[i].id === `${props.productData.name}container`) {
                    for (let j = i + 1; j < cosa.length; j++) {
                        cosa[j].style.margin = `20vh 0vh 0vh 18vh`;
                    }
                }
            }
            setContainerActualStyle({
                "width": "160vh",
                "height": "15vh",
                "margin": `${(props.index === 0) ? (10) : (20)}vh 0vh 0vh 18vh`,
                "border": "5px solid black ",
                "backgroundColor": "#2999c1",
                "borderRadius": "20%",
                "transition": "all 1s"
            })
            document.getElementById(`${props.productData.name}formData`).style.display = 'none';
            setDisableHover(false)
        }

    }




    const onChange = (e) => {
        setDataValue(prevStyle => ({
            ...prevStyle,
            [e.target.name]: e.target.value
        }));

    }

    const changeImg = () => {
        document.getElementById(`${props.productData.name}containerImg`).style.display = 'inline';
    }

    const saveImg = (e) => {
        document.getElementById(`${props.productData.name}containerImg`).style.display = 'none';
        setOldData(prevStyle => ({
            ...prevStyle,
            imgUrl: dataValue.imgUrl
        }));
    }

    const cancelImg = (e) => {
        document.getElementById(`${props.productData.name}containerImg`).style.display = 'none';
        setDataValue(prevStyle => ({
            ...prevStyle,
            imgUrl: oldData.imgUrl
        }));

    }



    const onMouseEnterFormElement = (e) => {
        const elementHTML = e.target.id.replace(dataValue.name,'')
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            [elementHTML]: ComponentsStyles.formElementsText.onHover
        }));
    }

    const onMouseLeaveFormElement = (e) => {
        const elementHTML = e.target.id.replace(dataValue.name,'')
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            [elementHTML]: ComponentsStyles.formElementsText.actualStyle
        }));
    }

    const changeData = (e) => {
        e.target.style.display = 'none';
        document.getElementById(`${e.target.id}Input`).style.display = 'inline';
    }

    const cleaningTheData = (data) => {
        try {
            return data = data.trim();
        } catch (error) {
            return ''
        }
    }

    const dispacherProducts = (response) => {
        if (response.status === 200) {
            window.location.href = '/managerProducts';
        }else{
            alert('Error.El nombre del producto ya esta en uso.')
        }
    }

    const validatingTheData = () => {
        const validator = { data: {}, boolean: false }


        validator.data.score = dataValue.score;
        validator.data.usersAlreadyScored = dataValue.usersAlreadyScored;
        validator.data.__v = dataValue.__v;
        validator.data._id = dataValue._id;

        if (dataValue.name.length === 0 || dataValue.name === oldData.name) {
            validator.data.name = oldData.name
            validator.boolean = validator.boolean || false
        } else {
            validator.data.name = dataValue.name
            validator.boolean = validator.boolean || true
        }


        if (dataValue.name.ingredients === 0 || dataValue.ingredients === oldData.ingredients) {
            validator.data.ingredients = oldData.ingredients
            validator.boolean = validator.boolean || false
        } else {
            validator.data.ingredients = dataValue.ingredients
            validator.boolean = validator.boolean || true
        }


        if (dataValue.name.imgUrl === 0 || dataValue.imgUrl === oldData.imgUrl) {
            validator.data.imgUrl = oldData.imgUrl
            validator.boolean = validator.boolean || false
        } else {
            validator.data.imgUrl = dataValue.imgUrl
            validator.boolean = validator.boolean || true
        }


        if (isNaN(dataValue.price) === true || dataValue.price === oldData.price) {
            validator.data.price = oldData.price
            validator.boolean = validator.boolean || false
        } else {
            validator.data.price = dataValue.price
            validator.boolean = validator.boolean || true
        }

        return validator
    }

    const updateProduct = async (dataInput) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(dataInput),
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/modProduct/?id=${dataInput._id}`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const deleteProduct = async (id) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'DELETE',
            headers: myHeaders,
            redirect: 'follow',
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/admin/deleteProduct/?id=${id}`, requestOptions)
            .then(res => res.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }




    const onDelete = () => {
        const userConfirm = window.confirm(`¿ESTA SEGURO/A QUE QUIERE ELIMINAR EL PRODUCTO?
        \n ESTA ACCION NO SE PUEDE DESHACER`);
        if (userConfirm === true) {
            deleteProduct(dataValue._id)
                .then(resolve => dispacherProducts(resolve))
                .catch(error => console.log(error))
        }
    }

    const onSubmit = () => {
        const productData = dataValue;
        productData.name = cleaningTheData(productData.name);
        productData.ingredients = cleaningTheData(productData.ingredients);
        productData.imgUrl = cleaningTheData(productData.imgUrl);
        productData.price = parseInt(productData.price)

        const validator = validatingTheData();

        if (validator.boolean === true) {
            updateProduct(validator.data)
                .then(resolve => dispacherProducts(resolve))
                .catch(error => console.log(error))
        } else {
            alert('Error.Has introducido datos invalidos.')
        }

    }

    return <div style={{ backgroundColor: '#3c3c3c', width: '0vh', height: '0vh' }}>
        <div key={`${props.productData.name}container`} id={`${props.productData.name}container`} style={containerActualStyle}
            onMouseEnter={() => { if (!disableHover) { onMouseEnterCard() } }}
            onMouseLeave={() => { if (!disableHover) { onMouseLeaveCard() } }}
            className={"productContainer"}
        >

            <img src={SeparatorImg} id={`${props.productData.name}separator`}
                style={{ width: '70vh', height: '8vh', margin: "0vh 0vh 0vh 45vh", display: 'inline', cursor: 'pointer' }}
                onClick={() => { onMouseClickCard() }}
            ></img>
            <h3 style={titleActualStyle} onClick={() => { onMouseClickCard() }}>{props.productData.name}</h3>



            <form onSubmit={(e) => { e.preventDefault() }} style={{ display: 'none' }}
                id={`${props.productData.name}formData`} key={`${props.productData.name}formData`}>

                <h3 id={`${props.productData.name}name`} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.name} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    NOMBRE :  {dataValue.name}</h3>


                <input id={`${props.productData.name}nameInput`} name={'name'} type='text' className={'dataInput'}
                    style={formElementsInputActualStyle.name}
                    onChange={onChange} value={dataValue.name}
                ></input>


                <h3 id={`${props.productData.name}ingredients`} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.ingredients} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    INGREDIENTES :  {dataValue.ingredients}</h3>

                <input id={`${props.productData.name}ingredientsInput`} name={'ingredients'} type='text' className={'dataInput'}
                    style={formElementsInputActualStyle.ingredients}
                    onChange={onChange} value={dataValue.ingredients}></input>

                <h3 id={`${props.productData.name}price`} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.price} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    PRECIO :  {dataValue.price}</h3>

                <input id={`${props.productData.name}priceInput`} name={'price'} type='number' className={'dataInput'}
                    style={formElementsInputActualStyle.price}
                    onChange={onChange} value={dataValue.price}></input>

                <h3 id={`${props.productData.name}img`} onDoubleClick={changeImg} className={'dataText'}
                    style={formElementsTextActualStyle.img} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    IMG URL :  {dataValue.imgUrl}</h3>


                <div id={`${props.productData.name}containerImg`} style={{
                    border: '0.3vh solid #000000',
                    width: "100vh",
                    height: "80vh",
                    backgroundColor: "#2999c1",
                    borderRadius: "20%",
                    position: 'absolute',
                    display: "none",
                    // margin: "-50vh 0vh 0vh 30vh"
                    "margin": `-55vh 0vh 0vh 30vh`,
                    zIndex: 300
                }}>


                    <img src={dataValue.imgUrl} style={{
                        width: "50vh",
                        height: "50vh",
                        margin: "2vh 0vh 0vh 23vh",
                        border: '0.3vh solid #000000',
                    }}></img>

                    <input id={'typeInput'} name={'imgUrl'} type='text'
                        style={{
                            "display": "inline",
                            "fontSize": "5vh",
                            "textAlign": "center",
                            "margin": "1vh 0vh 0vh 2vh",
                            "width": '90vh',
                            "backgroundColor": "#2999c1",
                            "borderRadius": "10%",
                            "color": "#000000",
                            "fontFamily": "'Montserrat', sans-serif",
                            "cursor": "pointer",
                            "transition": "all 0.5s"
                        }
                        }
                        onChange={onChange} value={dataValue.imgUrl}></input>

                    <button onClick={() => { saveImg() }}

                        style={
                            productButtonImgStyleSave
                        }

                        onMouseEnter={() => {
                            setProductButtonImgStyleSave({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                transition: "background-color 0.5s",
                                margin: "13vh 0vh 0vh -70vh",
                                fontSize: "40px",
                                display: 'inline',
                                position: 'absolute'
                            })
                        }
                        }
                        onMouseLeave={() => {
                            setProductButtonImgStyleSave({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                transition: "background-color 0.5s",
                                margin: "13vh 0vh 0vh -70vh",
                                fontSize: "40px",
                                display: 'inline',
                                position: 'absolute'
                            })
                        }
                        }

                    >GUARDAR</button>

                    <button onClick={() => { cancelImg() }}

                        style={
                            productButtonImgStyleCancel
                        }

                        onMouseEnter={() => {
                            setProductButtonImgStyleCancel({
                                "backgroundColor": "#15295f",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                transition: "background-color 0.5s",
                                margin: "13vh 0vh 0vh -40vh",
                                fontSize: "40px",
                                display: 'inline',
                                position: 'absolute'
                            })
                        }
                        }
                        onMouseLeave={() => {
                            setProductButtonImgStyleCancel({
                                "backgroundColor": "#4371ef",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                transition: "background-color 0.5s",
                                margin: "13vh 0vh 0vh -40vh",
                                fontSize: "40px",
                                display: 'inline',
                                position: 'absolute'
                            })
                        }
                        }

                    >CANCELAR</button>



                </div>

                <button onClick={() => { onSubmit() }}
                    onMouseEnter={() => {
                        setProductButtonStyleSend({
                            "backgroundColor": "#15295f",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "2vh 0vh 0vh 55vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute',
                        })
                    }
                    }
                    onMouseLeave={() => {
                        setProductButtonStyleSend({
                            "backgroundColor": "#4371ef",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "2vh 0vh 0vh 55vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute',
                        })
                    }
                    }
                    style={productButtonStyleSend} >ENVIAR</button>

                <button onClick={() => { onDelete() }}
                    onMouseEnter={() => {
                        setProductButtonStyleDelete({
                            "backgroundColor": "#15295f",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "2vh 0vh 0vh 85vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute',
                        })
                    }
                    }
                    onMouseLeave={() => {
                        setProductButtonStyleDelete({
                            "backgroundColor": "#4371ef",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            transition: "background-color 0.5s",
                            margin: "2vh 0vh 0vh 85vh",
                            fontSize: "40px",
                            display: 'inline',
                            position: 'absolute',
                        })
                    }
                    }
                    style={productButtonStyleDelete}>ELIMINAR</button>
            </form>


        </div>










    </div>
}


export default ProductsManagerData;
