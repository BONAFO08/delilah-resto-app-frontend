
import React, { useState } from "react";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";
import AdminItems from "./adminOptions.droplist.js";
import BasicItems from "./basicOptions.droplist";

function UserDropList(props) {
    const [dropdown, setDropdown] = useState(false);

    const abrirCerrar = () => {
        setDropdown(!dropdown);

    }

    const moveTo = (route) => {
        window.location.href = `/${route}`;
    }


    const showContent = () => {
        if (sessionStorage.getItem('sessionID') % 512 === 0) {
            return <AdminItems moveTo = {moveTo}/>
        }
    }

    const fetchLogOut =async ()=>{
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/logout`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const logOut =()=>{
        fetchLogOut()
        alert('ADIOS')
        sessionStorage.clear()
        window.location.href = `/`;
    }

    const defaultImg = 'https://upload.wikimedia.org/wikipedia/commons/d/d3/User_Circle.png';
    const userPanelImg = (sessionStorage.getItem('userImg') === null || sessionStorage.getItem('userImg') === 'undefined') ? (defaultImg) : (sessionStorage.getItem('userImg'))
    return <div>

        <span>
            <Dropdown isOpen={dropdown} toggle={abrirCerrar} id='banner'>
                <DropdownToggle style={{ backgroundColor: 'transparent', }}>
                    <h3 id="imgDrop" style={{ backgroundImage: `url(${userPanelImg})` }}></h3>
                </DropdownToggle>

                <DropdownMenu style={{ backgroundColor: 'black' }} >
                    <BasicItems moveTo={props.moveTo}></BasicItems>
                    {showContent()}
                    <DropdownItem onClick={()=>{logOut()}} className="drop-item">
                        LOGOUT
                    </DropdownItem>
                </DropdownMenu>

            </Dropdown>
        </span>
    </div >
}


export default UserDropList;


