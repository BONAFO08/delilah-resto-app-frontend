import React from "react";
import ShowAmmount from "../shop/showAmmount";
import './banner.css'

import UserDropList from "./userDropList.home";

class Banner extends React.Component {


    render() {
        return <div>

            <UserDropList moveTo= {this.props.moveTo}></UserDropList>

            <a className="bannerOptions" href="/home">INICIO</a>

            <a className="bannerOptions" href="/products">NUESTROS PRODUCTOS</a>

            <a className="bannerOptions" href="/doOrder">REALIZAR PEDIDO</a>

            <a className="bannerOptions" href="/stablishments">NUESTROS LOCALES</a>

            <a className="bannerOptions" href="/aboutus">ACERCA DE NOSOTROS</a>

        </div>
    }
}



export default Banner;


