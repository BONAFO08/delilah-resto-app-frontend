
import React from "react";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";

function AdminItems(props) {


    return <div>
        <DropdownItem onClick={()=>{props.moveTo('managerUser')}} className="drop-item">
            GESTOR USUARIOS
        </DropdownItem>
        <DropdownItem onClick={()=>{props.moveTo('managerProducts')}} className="drop-item">
            GESTOR PRODUCTOS
        </DropdownItem>
        <DropdownItem onClick={()=>{props.moveTo('managerPays')}} className="drop-item">
            GESTOR PAGOS
        </DropdownItem>
        <DropdownItem onClick={()=>{props.moveTo('managerOrders')}} className="drop-item">
            GESTOR ORDENES
        </DropdownItem>

    </div>
}


export default AdminItems;

