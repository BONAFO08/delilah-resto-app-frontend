import React, { useState, useEffect } from "react";
import HomeIndex from "./home.index";
import '../../Styles/crossComponent.login.home.css'

// INUTILIZADO!

// class CrossComponent extends React.Component {

//     state = {
//         component: ''

//     }

//     getPays = async () => {
//         let resp;
//         let myHeaders = new Headers();
//         myHeaders.append("Content-Type", "application/json");
//         myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

//         let requestOptions = {
//             method: 'GET',
//             headers: myHeaders,
//             redirect: 'follow'
//         };

//         await fetch("http://localhost:3000/user/payments", requestOptions)
//             .then(response => resp = response)
//             .catch(error => console.log('error', error));
//         return resp;
//     }

//     getProducts = async () => {
//         let resp;
//         let myHeaders = new Headers();
//         myHeaders.append("Content-Type", "application/json");
//         myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

//         let requestOptions = {
//             method: 'GET',
//             headers: myHeaders,
//             redirect: 'follow'
//         };

//         await fetch("http://localhost:3000/user/products", requestOptions)
//             .then(response => resp = response)
//             .catch(error => console.log('error', error));
//         return resp;
//     }

//     // verifyData = () => {
//     //     if (sessionStorage.getItem('saved')) {
//     //         return <HomeIndex payData={undefined} productData={2} validateUserToken={this.props.validateUserToken}></HomeIndex>
//     //     } else {
//     //         return <div className="welcome" id="welcome">
//     //             <h3 >BIENVENIDO A <br />DELILAH RESTRO</h3>
//     //             <button onClick={this.handleButtonClick}>INGRESAR</button>
//     //         </div>
//     //     }
//     // }

//     handleButtonClick = () => {
//         const fetchUserEmail = async () => {
//             const responsPays = await this.getPays();
//             const pay = await responsPays.json();
//             const responseProducts = await this.getProducts();
//             const products = await responseProducts.json();
//             document.getElementById('welcome').remove()
//             this.newElement(pay, products)
//             sessionStorage.setItem('saved', 'true')
//         };
//         fetchUserEmail();
//     };


//     newElement = (payData, productData) => {
//         this.setState({
//             component: <HomeIndex payData={payData} productData={productData} validateUserToken={this.props.validateUserToken}></HomeIndex>
//         })
//     }


//     render() {
//         return <div className="background-page">
//             <div >
//                 {this.state.component}
//             </div>
//             <div className="welcome" id="welcome">
//                 <h3 >BIENVENIDO A <br />DELILAH RESTRO</h3>
//                 <button onClick={this.handleButtonClick}>INGRESAR</button>
//             </div>
//         </div>
//     }
// }


function CrossComponent(props) {

    const [payData, setPayData] = useState('');
    const [component, setCopo] = useState('');

    const getPays = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch("http://localhost:3000/user/payments", requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const getProducts = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch("http://localhost:3000/user/products", requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const handleButtonClick = () => {
        const fetchUserEmail = async () => {
            const responsPays = await getPays();
            const pay = await responsPays.json();
            const responseProducts = await getProducts();
            const products = await responseProducts.json();
            newElement(pay, products)
        };
        fetchUserEmail();
    };

    const newElement = (payData, productData) => {
        setCopo(<HomeIndex payData={payData} productData={productData}></HomeIndex>)
    }

    useEffect(() => {
        props.validateUserToken()
       }, []
     );



    useEffect(() => {
        handleButtonClick()
       }, []
     );


    return <div>
        <div >
            {component}
        </div>
    </div>
}





export default CrossComponent;


