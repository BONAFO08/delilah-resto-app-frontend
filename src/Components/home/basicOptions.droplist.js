import React from "react";

import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";

class BasicItems extends React.Component {

    render() {
        return <div >
            <DropdownItem onClick={()=>{this.props.moveTo('myAccount')}} className="drop-item">
                MI CUENTA
            </DropdownItem>
            <DropdownItem onClick={()=>{this.props.moveTo('myOrders')}} className="drop-item">
                MIS PEDIDOS
            </DropdownItem>
        </div >
    }
}



export default BasicItems;


