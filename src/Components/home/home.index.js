import React from "react";
import PorductsHome from "../products/products.home.js";
import StablishmentsHome from "../stablishments/stablishments.home.js";
import Banner from "./banner.home";

class HomeIndex extends React.Component {



    render() {
        return <div className="background-page">
                {this.props.validateUserToken()}
                <Banner moveTo= {this.props.moveTo}></Banner>
                <PorductsHome moveTo={this.props.moveTo}></PorductsHome>
                <StablishmentsHome moveTo={this.props.moveTo}></StablishmentsHome>
        </div>
    }
}



export default HomeIndex;


