import React from "react";


class Pay extends React.Component {

    render() {
        // const { payData } = this.props
        // console.log(payData);
        // return <div key={`payName${payData[0].name}`}>{payData[0].imgUrl}</div>

        const { payData } = this.props
        return <div key={`${payData.name}_div`}>
            <h3 key={payData.name}>{payData.name}</h3>
            <h3 key={payData.imgUrl}>{payData.imgUrl}</h3>
        </div>

    }
}



export default Pay;


