
import React, { useState, useEffect } from "react";
import Pay from "./pay";

// class Pays extends React.Component {

//     state = {
//             payData: ''
//         };


//         salver = ()=>{
//             this.setState({
//                 payData : (this.props.payData === 1) ? (this.state.payData) : ('hola')
//             })
//         }
//         render() {
//             return (
//             <div>
//                 {/* <h1>A user</h1>
//                 <div>
//                 {this.state.perro}
//                 </div>
//                 <button  onClick={this.handleButtonClick}>LOGEARSE</button> */}
//             </div>
//         );
//     }
// }

function Pays() {
    const [payData, setPayData] = useState('');

    const showPay = () => {
        if (payData !== '') {
            return payData.map(pay=>  <Pay key={`${pay.name}PayComponent`} payData ={pay}></Pay>);
            
        }
    }

    const getPays = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/payments`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const handleFetchPayData = () => {
        const fetchPayData = async () => {
            const responsPays = await getPays();
            const pay = await responsPays.json();
            newElement(pay)
        };
        fetchPayData();
    };
    
    const newElement = (payData) => {
        console.log('payData',payData);
        setPayData(payData)
    }

    useEffect(() => {
        handleFetchPayData()
    }, []
    );
    
    return (
        <div>
            {showPay()}
        </div>
    );
}


export default Pays;