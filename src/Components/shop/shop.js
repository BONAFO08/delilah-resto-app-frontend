import React, { useState } from "react";
import AllProducts from "../products/products.products";
import ShopCart from "./shopCart";




function Shop(props) {

    const [wishList, setWishList] = useState(
        () => {
            let wishList = sessionStorage.getItem('cart')
            return wishList = JSON.parse(wishList);

        }
    );



    const addToCart = (producto) => {
        if (wishList === null) {
            producto.ammount = 1;
            producto.total = 1 * producto.price;
            sessionStorage.setItem('cart', JSON.stringify([producto]))
            setWishList([producto])
            window.location.href = "/products";

        } else {
            producto.ammount = 1;
            producto.total = 1 * producto.price;
            wishList.push(producto)
            sessionStorage.setItem('cart', JSON.stringify(wishList))
            setWishList(wishList)
            window.location.href = "/products";
        }

    }


    const addAmmountProduct = (_id) => {
        const seachNewProduct = wishList.filter(arr => arr._id === _id);
        const productIndex = wishList.indexOf(seachNewProduct[0]);
        wishList[productIndex].ammount += 1
        wishList[productIndex].total = wishList[productIndex].ammount * wishList[productIndex].price;
        sessionStorage.setItem('cart', JSON.stringify(wishList))
        setWishList(wishList)
        return wishList;
    }


    const reduceAmmountProduct = (_id) => {
        const seachNewProduct = wishList.filter(arr => arr._id === _id);
        const productIndex = wishList.indexOf(seachNewProduct[0]);

        if ((wishList[productIndex].ammount - 1) === 0) {
            const seachNewProduct = wishList.filter(arr => arr._id !== _id);
            sessionStorage.setItem('cart', JSON.stringify(seachNewProduct))
            setWishList(seachNewProduct)
            return seachNewProduct;
        } else {
            wishList[productIndex].ammount -= 1
            wishList[productIndex].total = wishList[productIndex].ammount * wishList[productIndex].price;
            sessionStorage.setItem('cart', JSON.stringify(wishList))
            setWishList(wishList)
            return wishList;
        }
    }


    const deleteAmmountProduct = (_id) => {
        const seachNewProduct = wishList.filter(arr => arr._id !== _id);
        sessionStorage.setItem('cart', JSON.stringify(seachNewProduct))
        setWishList(seachNewProduct)
        window.location.href = "/products";
        return seachNewProduct;
    }


    const showAmmount = () => {
        const ammounts = wishList.map(arr => arr.ammount);
        const sumAmmounts = ammounts.reduce(
            (previousValue, currentValue) => previousValue + currentValue,
            0
        );
        sessionStorage.setItem('ammounts',sumAmmounts)
    }



    // const showContent = (producto) => {
    //     if (wishList === null || wishList.filter(arr => arr.id === producto.id).length === 0) {
    //         return <button className={`${producto.name}addNewProductOption`} style={{ display: 'inline' }} onClick={() => { addToCart(producto) }}>AGREGAR AL CARRITO</button>
    //     } else {
    //         return <div>
    //             <button className={`${producto.name}updateProductOption`} style={{ display: 'inline' }} onClick={() => { addAmmountProduct(producto.id) }}>AUMENTAR CANTIDAD</button>
    //             <button className={`${producto.name}updateProductOption`} style={{ display: 'inline' }} onClick={() => { reduceAmmountProduct(producto.id) }}>DISMINUIR CANTIDAD</button>
    //             <button className={`${producto.name}updateProductOption`} style={{ display: 'inline' }} onClick={() => { setWishList(deleteAmmountProduct(producto.id)) }}>QUITAR DEL CARRITO</button>
    //         </div>

    //     }

    // }



    return <div>

        <ShopCart
            addAmmountProduct={addAmmountProduct}
            reduceAmmountProduct={reduceAmmountProduct}
            deleteAmmountProduct={deleteAmmountProduct}
            wishList={wishList}
        >

        </ShopCart>

        <AllProducts

            wishList={wishList}
            validateUserToken={props.validateUserToken}
            addToCart={addToCart}

        ></AllProducts>



    </div>


}


export default Shop;
