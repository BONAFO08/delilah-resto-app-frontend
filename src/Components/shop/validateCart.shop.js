import React, { useState, useEffect } from "react";


function ValidateCart(props) {
    const [finisher, setFinisher] = useState(0);
  
    const isTheShopCartEmpty = () => {
        const shopCart = window.sessionStorage.getItem('cart')
        if (shopCart === null) {
            window.location.href = '/products';
        }else{
            window.location.href = '/confirmOrder';
        }
    }


    useEffect(() => {
        props.validateUserToken()
        isTheShopCartEmpty()
        setFinisher(1)
    }, [finisher]
    )


    return <div>
    </div>
}


export default ValidateCart;
