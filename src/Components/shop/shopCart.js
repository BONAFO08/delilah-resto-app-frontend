


import React from "react";

import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";




class ShopCart extends React.Component {

    state = {
        dropdown: false,
        wishList: this.props.wishList,
        buttonsStyle: {

        }
    }

    abrirCerrar = () => {
        this.setState({
            dropdown: !this.state.dropdown
        })
    }


    showCart = (wishList) => {
        if (wishList !== null) {
            return wishList.map(producto => <DropdownItem onClick={(e) => e.target.preventDefault()} style={{ width: "300px", border: "2px solid red", backgroundColor: '#474fbb' }}>
                <img src={`${producto.imgUrl}`} style={{ width: "100px" }}></img>
                <h3>{producto.name}</h3>
                <h3>CANTIDAD : {producto.ammount}</h3>
                <h3>TOTAL: ${producto.total}</h3>
                <button onClick={() => {
                    this.setState({
                        wishList: this.props.addAmmountProduct(producto._id)
                    })
                }} onMouseEnter={(e) => e.target.style.backgroundColor = '#3f4be9'} onMouseLeave={(e) => e.target.style.backgroundColor = '#bdc4ff'}
                    style={
                        {
                            border: '2px solid #000000',
                            borderRadius: '50%',
                            fontSize: '30px',
                            padding: '0px 10px 0px 10px',
                            margin: '0px 0px 0px 5px',
                            backgroundColor: '#bdc4ff',
                            transition: 'background-color 0.4s'
                        }}>+</button>

                <button onClick={() => {
                    this.setState({
                        wishList: this.props.reduceAmmountProduct(producto._id)
                    })
                }} onMouseEnter={(e) => e.target.style.backgroundColor = '#3f4be9'} onMouseLeave={(e) => e.target.style.backgroundColor = '#bdc4ff'}
                    style={
                        {
                            border: '2px solid #000000',
                            borderRadius: '50%',
                            fontSize: '30px',
                            padding: '0px 15px 0px 15px'
                            , margin: '0px 0px 0px 5px',
                            backgroundColor: '#bdc4ff',
                            transition: 'background-color 0.4s'
                        }
                    }>-</button>

                <button onClick={() => {
                    this.setState({
                        wishList: this.props.deleteAmmountProduct(producto._id)
                    })
                }} onMouseEnter={(e) => e.target.style.backgroundColor = '#3f4be9'} onMouseLeave={(e) => e.target.style.backgroundColor = '#bdc4ff'}
                    style={
                        {
                            border: '2px solid #000000',
                            borderRadius: '50%',
                            fontSize: '30px',
                            padding: '0px 15px 0px 15px',
                            margin: '0px 0px 0px 5px',
                            backgroundColor: '#bdc4ff',
                            transition: 'background-color 0.4s'
                        }}>X</button>

            </DropdownItem>
            )
        }
    }


    render() {
        return <div>




            <Dropdown isOpen={this.state.dropdown} toggle={this.abrirCerrar} style={{ display: 'inline', position: 'absolute', margin: '-33.5vh 0vh 0vh 165vh', zIndex: 200, }}>

                <DropdownToggle style={{ backgroundColor: 'transparent' }}>
                    <img src={require('../../Images/shopCart.png')} style={{
                        width: "120px", margin: '0vh 0vh 0vh 0vh', borderRadius: "50%"
                    }} />

                </DropdownToggle>


                <DropdownMenu style={{ backgroundColor: '#000000' }} >

                    {this.showCart(this.state.wishList)}
                    <DropdownItem onClick={() => { window.location.href = '/doOrder' }}
                        onMouseEnter={(e) => e.target.style.backgroundColor = '#05afb2'} onMouseLeave={(e) => e.target.style.backgroundColor = '#474fbb'}
                        style={{ width: "300px", border: "2px solid #000000", backgroundColor: '#474fbb', transition: 'background-color 0.4s' }}>
                        CONFIRMAR ORDEN
                    </DropdownItem>

                </DropdownMenu>

            </Dropdown>



        </div>
    }

}

export default ShopCart;
