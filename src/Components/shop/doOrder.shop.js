import React, { useState, useEffect } from "react";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";


function DoOrder(props) {
    const [finisher, setFinisher] = useState(0);
    const [dropdownAddress, setDropdownAddress] = useState(false);
    const [dropdownPay, setDropdownPays] = useState(false);


    const [confirmOrderButtonStyle, setConfirmOrderButtonStyle] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 68vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "all 0.5s",
        fontSize: "40px"
    })

    const [cancelOrderButtonStyle, setCancelOrderButtonStyle] = useState({
        "width": "80vh",
        "height": "15vh",
        "margin": `10vh 0vh 0vh 68vh`,
        "border": "5px solid black ",
        "backgroundColor": "#175065",
        "borderRadius": "20%",
        transition: "background-color 0.5s",
        fontSize: "40px"
    })




    const [paypalButtonStyleSend, setPaypalButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        width: "80vh",
        height: "8vh",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "8vh 0vh 0vh 10vh",
        fontSize: "35px",
        display: 'block',
        transition: "all 0.5s"
    });

    const [cashButtonStyleSend, setCashButtonStyleSend] = useState({
        "backgroundColor": "#4371ef",
        width: "80vh",
        height: "8vh",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 10vh",
        fontSize: "35px",
        display: 'block',
        transition: "all 0.5s"
    });


    const [orderPayButtonStyleCancel, setOrderPayButtonStyleCancel] = useState({
        "backgroundColor": "#4371ef",
        width: "80vh",
        height: "8vh",
        border: "3px solid #000000",
        borderRadius: "10%",
        margin: "2vh 0vh 0vh 10vh",
        fontSize: "35px",
        display: 'block',
        transition: "all 0.5s"
    });



    const [orderID, setOrderID] = useState(0);
    const [userData, setUserData] = useState({});
    const [productData, setProductData] = useState([]);
    const [payData, setPayData] = useState([]);
    const [addressSelected, setAddressSelected] = useState({});
    const [paySelected, setPaySelected] = useState({});

    const openAndCloseAddress = () => {
        setDropdownAddress(!dropdownAddress);

    }

    const generateOrderID = () => {
        const num = Math.floor(Math.random() * 9999999999) + 1;
        setOrderID(num)
    }

    const cancelate = () => {
        document.getElementById('payContainer').style.display = 'none';
    }

    const showContainer = () => {
        document.getElementById('payContainer').style.display = 'inline';

    }


    const payForPaypal = async (orderData) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        orderData.orderID = orderID;
        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify({ order: orderData })
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/cosa`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const getUserData = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/showDataUser`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }

    const fetchUserData = async () => {
        const responseUserData = await getUserData();
        const userData = await responseUserData.json();
        setUserData(userData)
        setAddressSelected({name : userData.address[0], index : 0})
    };


    const validateData = () => {


        let totalAmmout = document.getElementById(`${userData.name}finalImport`).textContent;
        totalAmmout = totalAmmout.replace('IMPORTE TOTAL:  $', '');
        totalAmmout = parseInt(totalAmmout);


        if (addressSelected.name === '' || addressSelected.name === undefined) {
            return { bool: false, msj: 'Porfavor, elije una direccion valida.' }
        } else if (userData.phone === 0 || userData.phone === undefined) {
            return { bool: false, msj: 'Porfavor, proporciona en la pestaña de "MI CUENTA" un telefono valido.' }
        } else if (totalAmmout === 0 || isNaN(totalAmmout) === true) {
            return { bool: false, msj: 'Tu carrito esta vacio o todos los productos dentro de este son invalidos.Porfavor,reinicie el pedido.' }
        } else {
            return { bool: true, msj: '' }
        }
    }


    const createNewOrder = async (data) => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));
        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
            body: JSON.stringify(data)
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/newOrder/?idPay=${data.idPay}`, requestOptions)
            .then(response => response.json())
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const dispacherUser = (response) => {
        if (response.status === 200) {
            alert(`${response.msj}. \n Puede revisar el estado de su pedido en la opcion "Mis Pedidos" de su cuenta. `)
            window.sessionStorage.removeItem('cart')
            window.location.href = '/home';
        } else {
            alert(`Error. ${response.msj}`)
        }
    }

    const confirmOrderCash = () => {
        const dataStade = validateData();
        if (dataStade.bool !== false) {
            const cart = JSON.parse(window.sessionStorage.getItem('cart'))
            const products = cart.map(product => { return { id: product._id, ammount: product.ammount } })
            const idPay = (payData.filter(pay => pay.name === "Efectivo"))[0]

            const order = {
                arrFood: products,
                address: addressSelected.index + 1,
                idPay: idPay._id
            }
            createNewOrder(order)
                .then(resolve => dispacherUser(resolve))
                .catch(error => console.log(error))
        } else {
            alert(dataStade.msj)
        }
    }

    const confirmOrderPaypal = () => {
        // const dataStade = validateData();
        // if (dataStade.bool !== false) {
        const cart = JSON.parse(window.sessionStorage.getItem('cart'))
        const idPay = (payData.filter(pay => pay.name === "Paypal"))[0]

        const products = cart.map(product => {
            const tempPrice = product.price / process.env.REACT_APP_DOLLAR;
            return {
                name: product.name,
                price: `${tempPrice.toFixed(2)}`,
                currency: "USD",
                quantity: product.ammount,

            }
        })
        const idsProducts = cart.map(product => {
            return {
                id: product._id,
                ammount: product.ammount,
            }
        })

        const totalAmmout = totalImportCalculate();

        const order = {
            arrFood: products,
            address: addressSelected.index + 1,
            idPay: idPay._id,
            totalAmmout: `${(totalAmmout / process.env.REACT_APP_DOLLAR).toFixed(2)}`,
            idsProducts: idsProducts
        }
        return order
        // } else {
        //     alert(dataStade.msj)
        // }

    }

    const getProducts = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/products`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }


    const fetchProductData = async () => {
        const responseProducts = await getProducts();
        const products = await responseProducts.json();
        setProductData(products)
    };


    const getPays = async () => {
        let resp;
        let myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

        let requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        await fetch(`${process.env.REACT_APP_URL_SERVER}/user/payments`, requestOptions)
            .then(response => resp = response)
            .catch(error => console.log('error', error));
        return resp;
    }



    const fetchPayData = async () => {
        const responsePays = await getPays();
        const pays = await responsePays.json();
        setPayData(pays)
    };

    const createAddressDropList = () => {
        try {
            return userData.address.map((address, index) => <DropdownItem key={`${address}dropItem`} style={{ fontSize: '5vh' }} onClick={() => { setAddressSelected({ name: address, index: index }) }} className="drop-item">
                {address}
            </DropdownItem>)
        } catch (error) {
        }
    }


    const totalImportCalculate = () => {
        const cart = JSON.parse(window.sessionStorage.getItem('cart'))
        const bill = cart.map(product => { return { id: product._id, ammount: product.ammount } })


        const totalImport = bill.reduce((previousValue, currentValue) => {
            let price = productData.filter(product => product._id === currentValue.id)

            if (price.length !== 0) {
                price = price[0].price;
                return previousValue + price * currentValue.ammount
            } else {
                return previousValue
            }
        }, 0);
        return totalImport
    }



    const createFoodList = () => {
        const cart = JSON.parse(window.sessionStorage.getItem('cart'))
        const styleH3 = {
            "fontSize": "5vh",
            "textAlign": "center",
            "color": "#9196c0",
            "fontFamily": "'Montserrat', sans-serif",
            "transition": "all 0.5s",
            "margin": "5vh 0vh 5vh 0vh",
            "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
        };
        try {
            return cart.map(food => {
                const foodData = productData.filter(product => product._id === food._id)[0];

                if (foodData !== undefined) {
                    return <div key={`${foodData.name}div`} style={{ border: '0.5vh solid #000000' }}>
                        <h3 style={styleH3} key={`${foodData.name}name`}><span style={{ color: '#5f67dd', cursor: 'auto' }} >PRODUCTO:</span> {foodData.name}</h3>
                        <h3 style={styleH3} key={`${foodData.name}ammount`}><span style={{ color: '#5f67dd', cursor: 'auto' }} >CANTIDAD:</span> {food.ammount} UNIDADES</h3>
                        <h3 style={styleH3} key={`${foodData.name}price`}><span style={{ color: '#5f67dd', cursor: 'auto' }} >PRECIO UNITARIO:</span> ${foodData.price}</h3>
                    </div>
                }
            }
            )
        } catch (error) {
        }
    }

    const cancelateOrder = () => {
        const userConfirm = window.confirm('¿Esta seguro/a de cancelar el pedido? \n ESTO REINICIARA SU CARRITO')
        if (userConfirm === true) {
            window.sessionStorage.removeItem('cart')
            window.location.href = '/home';
        }
    }


    useEffect(() => {
        props.validateUserToken()
        generateOrderID()
        fetchUserData()
        fetchProductData()
        fetchPayData()
        setFinisher(1)
    }, [finisher]
    )

    return <div>


        <h3 id={`${userData.name}username`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >NOMBRE DE USUARIO :</span>   {userData.username}</h3>

        <h3 id={`${userData.name}name`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >NOMBRE Y APELLIDO :</span>   {userData.name}</h3>

        <h3 id={`${userData.name}email`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >CORREO ELECTRONICO :</span>   {userData.email}</h3>


        <h3 id={`${userData.name}phone`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >TELEFONO:</span>   {userData.phone}</h3>

        <h3 id={`${userData.name}address`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >DOMICILIO:</span>   {addressSelected.name}</h3>



        <Dropdown isOpen={dropdownAddress} toggle={openAndCloseAddress}
            style={{
                margin: "8vh 0vh 0vh 75vh",
            }}>
            <DropdownToggle style={{
                backgroundColor: '#60bfb2',
                padding: "1vh",
                fontSize: "5vh",
                "color": "#5f67dd",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"

            }}>
                SELECCIONAR DOMICILIO
            </DropdownToggle>
            <DropdownMenu style={{ backgroundColor: 'black' }} >
                {createAddressDropList()}
            </DropdownMenu>

        </Dropdown>

        <h3 id={`${userData.name}cart`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >COMIDA:</span></h3>


        {createFoodList()}

        <h3 id={`${userData.name}finalImport`} className={'dataText'}
            style={{
                "fontSize": "5vh",
                "textAlign": "center",
                "color": "#9196c0",
                "fontFamily": "'Montserrat', sans-serif",
                "transition": "all 0.5s",
                "margin": "10vh 0vh 0vh 0vh",
                "textShadow": "2px 2px 2px #000000, 2px 2px 2px #000000, 2px 2px 2px #000000, 2px -1px 2px #000000, -1px -1px 2px #000000, -1px 4px 0px #000000"
            }}
        >
            <span style={{ color: '#5f67dd', cursor: 'auto' }} >IMPORTE TOTAL: </span> ${totalImportCalculate()}</h3>


        <button
            id="confirmOrderButton"
            onClick={() => showContainer()}
            onMouseEnter={() => {


                setConfirmOrderButtonStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 28vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#18bd55",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "50px"
                })

            }}

            onMouseLeave={() => {
                setConfirmOrderButtonStyle({
                    "width": "80vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 68vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "40px"
                })
            }}
            style={confirmOrderButtonStyle}
        >CONFIRMAR PEDIDO</button>


        <button
            id="cancelOrderButton"
            onClick={() => { cancelateOrder() }}
            onMouseEnter={() => {
                setCancelOrderButtonStyle({
                    "width": "160vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 28vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#cf1d1d",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "50px"
                })
            }}

            onMouseLeave={() => {
                setCancelOrderButtonStyle({
                    "width": "80vh",
                    "height": "15vh",
                    "margin": `10vh 0vh 0vh 68vh`,
                    "border": "5px solid black ",
                    "backgroundColor": "#175065",
                    "borderRadius": "20%",
                    transition: "all 0.5s",
                    fontSize: "40px"
                })
            }}
            style={cancelOrderButtonStyle}
        >CANCELAR PEDIDO</button>


        <div
            id={"payContainer"}
            style={{
                border: '0.3vh solid #000000',
                width: "100vh",
                height: "60vh",
                backgroundColor: "#2999c1",
                borderRadius: "20%",
                position: 'absolute',
                display: "none",
                "margin": `-40vh 0vh 0vh -90vh`,
            }}>


            <form action={`http://localhost:3000/paypal?orderID=${orderID}`} onSubmit={() => { payForPaypal(confirmOrderPaypal()) }} method="post">
                <h3
                    style={{
                        margin: "0% 18%",
                        fontSize: "40px"
                    }}
                >SELECCIONE UN METODO DE PAGO</h3>
                <input
                    onMouseEnter={
                        () => {
                            setPaypalButtonStyleSend({
                                "backgroundColor": "#1c1282",
                                width: "80vh",
                                height: "8vh",
                                border: "3px solid #000000",
                                color: "#ffffff",
                                borderRadius: "10%",
                                margin: "8vh 0vh 0vh 10vh",
                                fontSize: "35px",
                                display: 'block',
                                transition: "all 0.5s"
                            })
                        }
                    }
                    onMouseLeave={
                        () => {
                            setPaypalButtonStyleSend({
                                "backgroundColor": "#4371ef",
                                width: "80vh",
                                height: "8vh",
                                border: "3px solid #000000",
                                borderRadius: "10%",
                                margin: "8vh 0vh 0vh 10vh",
                                fontSize: "35px",
                                display: 'block',
                                transition: "all 0.5s"
                            })
                        }
                    }
                    style={paypalButtonStyleSend}
                    type="submit" value="PAYPAL"></input>




            </form>
            <button
                onClick={() => confirmOrderCash()}
                onMouseEnter={
                    () => {
                        setCashButtonStyleSend({
                            "backgroundColor": "#06612e",
                            width: "80vh",
                            height: "8vh",
                            textAlign: "center",
                            margin: "2vh 0vh 0vh 10vh",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            fontSize: "35px",
                            display: 'block',
                            transition: "all 0.5s"
                        })
                    }
                }
                onMouseLeave={
                    () => {
                        setCashButtonStyleSend({
                            "backgroundColor": "#4371ef",
                            width: "80vh",
                            height: "8vh",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            margin: "2vh 0vh 0vh 10vh",
                            fontSize: "35px",
                            display: 'block',
                            transition: "all 0.5s"
                        })
                    }
                }
                style={cashButtonStyleSend}
            >EFECTIVO</button>


            <button
                onClick={() => cancelate()}
                onMouseEnter={
                    () => {
                        setOrderPayButtonStyleCancel({
                            "backgroundColor": "#cf1d1d",
                            width: "80vh",
                            height: "8vh",
                            textAlign: "center",
                            margin: "2vh 0vh 0vh 10vh",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            fontSize: "35px",
                            display: 'block',
                            transition: "all 0.5s"
                        })
                    }
                }
                onMouseLeave={
                    () => {
                        setOrderPayButtonStyleCancel({
                            "backgroundColor": "#4371ef",
                            width: "80vh",
                            height: "8vh",
                            border: "3px solid #000000",
                            borderRadius: "10%",
                            margin: "2vh 0vh 0vh 10vh",
                            fontSize: "35px",
                            display: 'block',
                            transition: "all 0.5s"
                        })
                    }
                }
                style={orderPayButtonStyleCancel}
            >CANCELAR</button>

        </div>

        {/* <form action={`http://localhost:3000/paypal?orderID=${orderID}`} onSubmit={() => { payForPaypal(confirmOrderPaypal()) }} method="post">
            <input style={{
                "width": "80vh",
                "height": "15vh",
                "margin": `10vh 0vh 0vh 68vh`,
                "border": "5px solid black ",
                "backgroundColor": "#175065",
                "borderRadius": "20%",
                transition: "all 0.5s",
                fontSize: "40px"
            }} type="submit" value="Buy"></input>
        </form> */}
    </div>
}


export default DoOrder;
