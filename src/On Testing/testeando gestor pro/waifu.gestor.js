import React, { useState } from "react";
import ComponentsStyles from './waifu.gestor.style.json'
import SeparatorImg from './separator.png'

function WaifuGestor(props) {
    const [containerActualStyle, setContainerActualStyle] = useState(ComponentsStyles.container.actualStyle)
    const [titleActualStyle, setTitleActualStyle] = useState(ComponentsStyles.title.actualStyle)

    const [formElementsTextActualStyle, setformElementsTextActualStyle] = useState({
        name: ComponentsStyles.formElementsText.actualStyle,
        address: ComponentsStyles.formElementsText.actualStyle,
        img: ComponentsStyles.formElementsText.actualStyle,
        hobby: ComponentsStyles.formElementsText.actualStyle,
        type: ComponentsStyles.formElementsText.actualStyle,
    })

    const [formElementsInputActualStyle, setformElementsInputActualStyle] = useState({
        name: ComponentsStyles.formElementsInputs.actualStyle,
        address: ComponentsStyles.formElementsInputs.actualStyle,
        img: ComponentsStyles.formElementsInputs.actualStyle,
        hobby: ComponentsStyles.formElementsInputs.actualStyle,
        type: ComponentsStyles.formElementsInputs.actualStyle,
    })


  

    const [dataValue, setDataValue] = useState(props.data)
    const [disableHover, setDisableHover] = useState(false)


    const onMouseEnterCard = () => {
        setContainerActualStyle(ComponentsStyles.container.onHover)

    }

    const onMouseLeaveCard = () => {
        setContainerActualStyle(ComponentsStyles.container.actualStyle)
    }

    const onMouseClickCard = () => {
        if (!disableHover) {
            setContainerActualStyle(ComponentsStyles.container.onClick)
            document.getElementById(`${props.data.name}formData`).style.display = 'inline';
            setDisableHover(true)
        } else {
            setContainerActualStyle(ComponentsStyles.container.actualStyle)
            document.getElementById(`${props.data.name}formData`).style.display = 'none';
            setDisableHover(false)
        }

    }




    const onChange = (e) => {
        setDataValue(prevStyle => ({
            ...prevStyle,
            [e.target.name]: e.target.value
        }));

    }

    const changeImg = () => {
        document.getElementById('containerImg').style.display = 'inline';
    }

    const saveImg = (e) => {
        document.getElementById('containerImg').style.display = 'none';
    }


    const onMouseEnterFormElement = (e) => {
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            [e.target.id]: ComponentsStyles.formElementsText.onHover
        }));
    }

    const onMouseLeaveFormElement = (e) => {
        setformElementsTextActualStyle(prevStyle => ({
            ...prevStyle,
            [e.target.id]: ComponentsStyles.formElementsText.actualStyle
        }));
    }

    const changeData = (e) => {
        e.target.style.display = 'none';
        document.getElementById(`${e.target.id}Input`).style.display = 'inline';
    }

    const onSubmit = (e) => {
        e.preventDefault();

        const textElements = document.getElementsByClassName('dataText');
        const inputElements = document.getElementsByClassName('dataInput');


        for (let i = 0; i < textElements.length; i++) {
            textElements[i].style.display = 'block'
            inputElements[i].style.display = 'none'
        }
    }

    return <div style={{ backgroundColor: '#3c3c3c', width: '500vh', height: '500vh' }}>
        <div key={`${props.data.name}container`} id={`${props.data.name}container`} style={containerActualStyle}
            onMouseEnter={() => { if (!disableHover) { onMouseEnterCard() } }}
            onMouseLeave={() => { if (!disableHover) { onMouseLeaveCard() } }}
        >

            <img src={SeparatorImg} id={`${props.data.name}separator`}
                style={{ width: '70vh', height: '8vh', margin: "0vh 0vh 0vh 45vh", display: 'inline', cursor: 'pointer' }}
                onClick={() => { onMouseClickCard() }}
            ></img>
            <h3 style={titleActualStyle} onClick={() => { onMouseClickCard() }}>{props.data.name}</h3>



            <form onSubmit={onSubmit} style={{ display: 'none' }}
                id={`${props.data.name}formData`} key={`${props.data.name}formData`}>

                <h3 id={'name'} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.name} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    NOMBRE :  {dataValue.name}</h3>


                <input id={'nameInput'} name={'name'} type='text' className={'dataInput'}
                    style={formElementsInputActualStyle.name}
                    onChange={onChange} value={dataValue.name}></input>


                <h3 id={'address'} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.address} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    DOMICILIO :  {dataValue.address}</h3>

                <input id={'addressInput'} name={'address'} type='text' className={'dataInput'}
                    style={formElementsInputActualStyle.address}
                    onChange={onChange} value={dataValue.address}></input>

                <h3 id={'hobby'} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.hobby} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    HOBBY :  {dataValue.hobby}</h3>

                <input id={'hobbyInput'} name={'hobby'} type='text' className={'dataInput'}
                    style={formElementsInputActualStyle.hobby}
                    onChange={onChange} value={dataValue.hobby}></input>

                <h3 id={'type'} onDoubleClick={changeData} className={'dataText'}
                    style={formElementsTextActualStyle.type} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    TIPO :  {dataValue.type}</h3>

                <input id={'typeInput'} name={'type'} type='text' className={'dataInput'}
                    style={formElementsInputActualStyle.type}
                    onChange={onChange} value={dataValue.type}></input>

                <h3 id={'img'} onDoubleClick={changeImg} className={'dataText'}
                    style={formElementsTextActualStyle.img} onMouseEnter={onMouseEnterFormElement} onMouseLeave={onMouseLeaveFormElement}
                >
                    IMG URL :  {dataValue.img}</h3>


                <div id="containerImg" style={{
                    border: '0.3vh solid #000000',
                    width: "100vh",
                    height: "80vh",
                    backgroundColor: "#2999c1",
                    borderRadius: "20%",
                    position : 'absolute',
                    display : "none" ,
                    margin: "-50vh 0vh 0vh 30vh"
                }}>


                        <img src={dataValue.img} style={{
                            width: "50vh",
                            height: "50vh",
                            margin: "2vh 0vh 0vh 23vh",
                            border: '0.3vh solid #000000',
                        }}></img>

                        <input id={'typeInput'} name={'img'} type='text' 
                            style={{
                                "display": "inline",
                                "fontSize": "5vh",
                                "textAlign": "center",
                                "margin": "1vh 0vh 0vh 2vh",
                                "width": '90vh',
                                "backgroundColor": "#2999c1",
                                "borderRadius": "10%",
                                "color": "#000000",
                                "fontFamily": "'Montserrat', sans-serif",
                                "cursor": "pointer",
                                "transition": "all 0.5s"
                            }
                            }
                            onChange={onChange} value={dataValue.img}></input>
                    <button onClick={()=>{saveImg()}}>GUARDAR</button>

                </div>


                <input type='submit' value='GUARDAR'></input>
            </form>

        </div>










    </div>
}


export default WaifuGestor;
