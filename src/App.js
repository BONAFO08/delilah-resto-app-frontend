import React from 'react';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';


import LoginIndex from './Components/index/login.index';
import Login from './Components/index/login.login';
import Signup from './Components/index/login.signup';


import './Styles/background.css'

import 'bootstrap/dist/css/bootstrap.min.css'
import HomeIndex from './Components/home/home.index';
import Banner from './Components/home/banner.home';
import AllStablishments from './Components/stablishments/stablishments.stablishments';
import Shop from './Components/shop/shop';
import ProductsManager from './Components/managers/managerProducts/products.manager';
import PaysManager from './Components/managers/managerPays/pay.manager';
import UserManager from './Components/managers/managerUser/user.manager';
import GoogleRedirect from './Components/index/redirect.google';
import MyAccount from './Components/myAccount/myAccount.data';
import DoOrder from './Components/shop/doOrder.shop';
import ValidateCart from './Components/shop/validateCart.shop';
import SucessPaypal from './Components/shop/sucessPaypal.shop';
import CancelPaypal from './Components/shop/CancelPaypal.shop';
import MyOrders from './Components/myOrders/myOrders';
import OrdersManager from './Components/managers/managerOrders/order.manager';


class App extends React.Component {

  validator = async () => {
    let resp;
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", window.sessionStorage.getItem('token'));

    let requestOptions = {
      method: 'POST',
      headers: myHeaders,
      redirect: 'follow'
    };


    await fetch(`${process.env.REACT_APP_URL_SERVER}/user/validateToken`, requestOptions)
      .then(response => resp = response.json())
      .catch(error => console.log('error', error));
    return resp;
  }

  validateUserToken = () => {
    try {
      if (window.sessionStorage.getItem('token') === undefined) {
        window.location.href = '/';
      } else {
        const response = this.validator()
          .then(response => {
            if (response.status !== 200) {
              window.location.href = '/';
            }
          })
          .catch(error => {
            window.location.href = '/';
          });
      }
    } catch (error) {
      window.location.href = '/';
    }
  }

  moveTo = (route) => {
    window.location.href = `/${route}`;
  }


  render() {

    return (
      <div >
        <Router>
          <Routes>
            <Route exact path='/' element={<div>
              <LoginIndex></LoginIndex>
            </div>}>
            </Route>

            <Route exact path='/login' element={<div>
              <Login></Login>
            </div>}>
            </Route>

            <Route exact path='/signup' element={<div>
              <Signup></Signup>
            </div>}>
            </Route>

            <Route exact path='/home' element={
              <div>
                <HomeIndex validateUserToken={this.validateUserToken} moveTo={this.moveTo} ></HomeIndex>
              </div>}>
            </Route>

            <Route exact path='/products' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>

                <Shop validateUserToken={this.validateUserToken}></Shop>

              </div>}>
            </Route>

            <Route exact path='/stablishments' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <AllStablishments validateUserToken={this.validateUserToken} ></AllStablishments>
              </div>}>
            </Route>

            <Route exact path='/managerProducts' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <ProductsManager validateUserToken={this.validateUserToken}></ProductsManager>
              </div>}>
            </Route>

            <Route exact path='/managerPays' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <PaysManager validateUserToken={this.validateUserToken}></PaysManager>
              </div>}>
            </Route>


            <Route exact path='/managerUser' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <UserManager validateUserToken={this.validateUserToken}></UserManager>
              </div>}>
            </Route>

            <Route exact path='/managerOrders' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
              <OrdersManager validateUserToken={this.validateUserToken}></OrdersManager>
              </div>}>
            </Route>

            <Route exact path='/myAccount' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <MyAccount validateUserToken={this.validateUserToken}></MyAccount>
              </div>}>
            </Route>


            <Route exact path='/doOrder' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <ValidateCart validateUserToken={this.validateUserToken}></ValidateCart>
              </div>}>
            </Route>

            <Route exact path='/sucessPaypal' element={
              <div className="background-page">
                <SucessPaypal></SucessPaypal>
              </div>}>
            </Route>

            <Route exact path='/cancelPaypal' element={
              <div className="background-page">
                <CancelPaypal></CancelPaypal>
              </div>}>
            </Route>

            <Route exact path='/confirmOrder' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
                <DoOrder validateUserToken={this.validateUserToken}></DoOrder>
              </div>}>
            </Route>

            <Route exact path='/testenado/:token' element={
              <div className="background-page">
                <GoogleRedirect></GoogleRedirect>
              </div>}>
              </Route>
              
            <Route exact path='/myOrders' element={
              <div className="background-page">
                <Banner moveTo={this.moveTo}></Banner>
              <MyOrders validateUserToken={this.validateUserToken}></MyOrders>
              </div>}>
            </Route>
      


          </Routes>
        </Router>
      </div>
    )
  }
}




export default App;
